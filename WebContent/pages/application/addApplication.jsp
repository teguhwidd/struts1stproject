<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/application">
	<span class="h2-judul">Create Application</span> <<%-- bean:write name="applicationForm" property="custId"/> --%>
	<!-- Application Information -->
		<table border="1" class="input">
			<tr>
				<th colspan="2">Application Information</th>
			</tr>
			<tr>
				<td class="inlabel">Referal Branch</td>
				<td><html:text property="refferalBranch" maxlength="40" value="0960 - Unit Bisnis Kartu Kredit" disabled="true"/></td>
			</tr>
			<tr>
				<td class="inlabel">Date Received</td>
				<td>
				<html:text property="dateReceived" styleId="dateReceived" styleClass="kecil" maxlength="10"/><span>&nbsp;(DD/MM/YYYY)</span>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Facility</td>
				<td>
					<html:select property="facility">
						<html:option value=""></html:option>
						<html:option value="500-PL Asuransi Jiwa">500-PL Asuransi Jiwa</html:option>
						<html:option value="500-PL Asuransi Jiwa + PHK">600-PL Asuransi Jiwa + PHK</html:option>
					</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Application Purpose</td>
				<td>
				<html:select property="applicationPurpose">
					<html:option value=""></html:option>
					<html:option value="Sekolah">Sekolah</html:option>
					<html:option value="Liburan">Liburan</html:option>
					<html:option value="Modal Kerja">Modal Kerja</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Business Source</td>
				<td><html:text property="businessSource" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">Media Channel</td>
				<td>
					<html:select property="mediaChannel">
						<html:option value=""></html:option>
						<html:option value="TV">TV</html:option>
						<html:option value="Surat Kabar">Surat Kabar</html:option>
						<html:option value="Internet">Internet</html:option>
						<html:option value="Cabang">Cabang</html:option>
					</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Fee Branch</td>
				<td><html:text property="feeBranch" maxlength="40" value="0960 - Unit Bisnis Kartu Kredit" disabled="true"/></td>
			</tr>
			<tr>
				<td class="inlabel">Provision Branch</td>
				<td><html:text property="provisionBranch" maxlength="40" value="0960 - Unit Bisnis Kartu Kredit" disabled="true"/></td>
			</tr>
			<tr>
				<td class="inlabel">KCKK Branch</td>
				<td><html:text property="kckkBranch" maxlength="40" value="0960 - Unit Bisnis Kartu Kredit" disabled="true"/></td>
			</tr>
			<tr>
				<td colspan="2" class="th">Staff Information</td>
			</tr>
			<tr>
				<td class="inlabel">Staffs Name</td>
				<td><html:text property="staffName" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">Staffs NIP No.</td>
				<td>
				<html:text property="staffNip" styleId="dob" styleClass="sedang" maxlength="10"/>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Staffs Branch</td>
				<td><html:text property="staffBranch" maxlength="40" value="0960 - Unit Bisnis Kartu Kredit" disabled="true"/></td>
			</tr>
			<tr>
				<td class="inlabel">Staffs Account No.</td>
				<td>
				<html:text property="staffAccountNum" styleId="dob" styleClass="kecil" maxlength="10"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<html:hidden property="task" value="doAdd"/>
				<input type="submit" value="Save" /> &nbsp; 
				<a href="javascript:backPage('application.do?task=showApplication');"><input type="button" value="Cancel" /></a>
				</td>
			</tr>
		</table>
		</html:form>	
		<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
		<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
		<script>
		function backPage(src) {
			parent.content.location = src;
		}
	
	</script>
</body>
</html>