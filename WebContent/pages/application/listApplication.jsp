<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<div class="alert alert-success center none"><i class="fa fa-check-circle-o fa-fw"></i>Pesan</div>
		<table border="1" class="view">
			<tr>
				<th colspan="5" class="top">Customer Application List</th>
			</tr>
			<tr>
				<th >Ref No.</th>
				<th >Date Created</th>
				<th >Creator</th>
				<th >Status</th>
				<th >Hold By</th>
			</tr>
			<logic:empty name="applicationForm" property="listApplication">
			<tr>
				<td colspan="5" class="center blue">Data is not found</td>
			</tr>
			</logic:empty>
			<logic:notEmpty name="applicationForm" property="listApplication">
			<logic:iterate id="idlist" name="applicationForm" property="listApplication">
				<tr>
					<td><a href="javascript:openDetail('<bean:write name="idlist" property="refNo"/>')"><bean:write name="idlist" property="refNo" /></a></td>
					<td><bean:write name="idlist" property="dateReceived" /></td>
					<td><bean:write name="idlist" property="holdBy" /></td>
					<td><bean:write name="idlist" property="status" /></td>
					<td><bean:write name="idlist" property="holdBy" /></td>
				</tr>
			</logic:iterate>
			<tr>
				<td colspan="5" class="infoot">
				<input type="button" class="nav left" value="<<"/> &nbsp; 
				<input type="button" class="nav left" value="<"/> &nbsp; 
				<input type="button" class="nav left" value=">"/> &nbsp; 
				<input type="button" class="nav left" value=">>"/> &nbsp;
				<span class="search left">&nbsp;&nbsp;&nbsp;Page&nbsp;</span>
				<input type="text" class="search left"/>
				<input type="button" class="nav left" value="Go"/> &nbsp;
				<span class="search right">&nbsp;&nbsp;&nbsp;Total 1000 Record(s), Page 1 of 2&nbsp;</span>
			</tr>
			</logic:notEmpty>
			<tr>
				<td colspan="5" class="infoot">
				<a href="javascript:addPage('application.do?task=showAdd');"><input type="button" value="Add" /></a>
				</td>
			</tr>
		</table>
		<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		function searchPage(){
			document.forms[0].task.value="doSearch";
			document.forms[0].submit();
		}
		function addPage(src) {
			parent.content.location = src;
		}
		
		</script>
</body>
</html>