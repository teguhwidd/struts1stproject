<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/city" styleId="formCity" >
		<table border="1" class="input">
			<tr>
				<th colspan="2">City Information</th>
			</tr>
			<tr>
				<td class="inlabel">City Id</td>
				<td><html:text property="cityId" value=""/></td>
			</tr>
			<tr>
				<td class="inlabel">City Name</td>
				<td><html:text property="cityName" value=""/></td>
			</tr>
			<tr>
				<td class="inlabel">Province ID</td>
				<td><html:text property="provinceId" value=""/></td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<html:hidden property="task" value="doAdd"/>
				<input type="submit" value="Save" /> &nbsp; 
				<a href="javascript:backCity('city.do?task=showCity');"><input type="button" value="Back" /></a>
				</td>
			</tr>
		</table>
	</html:form>
		<!-- <a href="#" class="eng">coba</a> -->
		
		<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
		<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
		<script type="text/javascript">
		var i1 = "City ID";
		var i2 = "City Name";
		var i2 = "Province ID";
		$(document).ready(function () {
			$("input[name=cityId]").focus();
		    $('#formCity').validate({
		    	rules: {
		    		cityId: {required: true},
		    		cityName: {required: true},
		    		provinceId: {required: true}
	            },
	            messages: {
	            	cityId: {required: i1+" harus diisi !!!"},
	            	cityName: {required: i2+" harus diisi !!!"},
	            	provinceId: {required: i3+" harus diisi !!!"},
	            },
	            /* tooltip_options: {
	            	coba: {placement:'right', html:true}
            	}, */
	            submitHandler: function(form) { form.submit(); }
		    });
		});
		function backCity(src) {
			parent.content.location = src;
		}
	
	</script>
</body>
</html>