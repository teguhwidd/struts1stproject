<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/city">
	<table border="1" class="input">
			<tr>
				<th colspan="2" class="top">Enter Search Criteria</th>
			</tr>
			<tr>
				<td class="inlabel">City Name</td>
				<td><html:text property="searchCityName"/></td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="Refresh" onclick="doRefresh()"/> &nbsp;<input type="button" value="Search" onclick="searchPage()"/> &nbsp; 
				</td>
				
			</tr>
		</table>
		
		<table border="1" class="view">
			<tr>
				<th colspan="3" class="top">City List</th>
			</tr>
			<tr>
				<th>Code</th>
				<th>Description</th>
				<th>Opsi</th>
			</tr>
			<logic:empty name="cityForm" property="listCity">
			<tr>
				<td colspan="3" class="center blue">Data is not found</td>
			</tr>
			</logic:empty>
			<logic:notEmpty name="cityForm" property="listCity">
			<logic:iterate id="idlist" name="cityForm" property="listCity">
				<tr>
					<td><a href="javascript:openDetail('<bean:write name="idlist" property="cityId"/>');"><bean:write name="idlist" property="cityId" /></a></td>
					<td><bean:write name="idlist" property="cityName" /></td>
					<td>
					<button class="btn bblue" onclick="showEdit('<bean:write name="idlist" property="cityId"/>')">Edit</button>
					<button class="btn bred" onclick="showDelete('<bean:write name="idlist" property="cityId"/>')">Delete</button>
					</td>
				</tr>
			</logic:iterate>
			<tr>
				<td colspan="3" class="infoot">
				<input type="button" class="nav left" value="<<"/> &nbsp; 
				<input type="button" class="nav left" value="<"/> &nbsp; 
				<input type="button" class="nav left" value=">"/> &nbsp; 
				<input type="button" class="nav left" value=">>"/> &nbsp;
				<span class="search left">&nbsp;&nbsp;&nbsp;Page&nbsp;</span>
				<input type="text" class="search left"/>
				<input type="button" class="nav left" value="Go"/> &nbsp;
				<span class="search right">&nbsp;&nbsp;&nbsp;Total 1000 Record(s), Page 1 of 2&nbsp;</span>
			</tr>
			<tr>
				<td colspan="8" class="infoot">
				<a href="javascript:addPage('city.do?task=showAdd');"><input type="button" value="Add" /></a>
				</td>
			</tr>
			</logic:notEmpty>
		</table>
		<html:hidden property="task"/>
		<html:hidden property="cityId"/>
		</html:form>
		<script type="text/javascript">
		function doRefresh(){
			document.forms[0].action="city.do";
			document.forms[0].task.value="showCity";
			document.forms[0].submit();
		}
		function showEdit(code) {
			document.forms[0].task.value = "showEdit";
			document.forms[0].cityId.value = code;
			document.forms[0].submit();
		}
		function showDelete(code){
			document.forms[0].task.value = "doDelete";
			document.forms[0].cityId.value = code;
			document.forms[0].submit();
		}
		function searchPage(){
			document.forms[0].action="city.do";
			document.forms[0].task.value="doSearch";
			document.forms[0].submit();
		}
		function addPage(src) {
			parent.content.location = src;
		}
		
		</script>
</body>
</html>