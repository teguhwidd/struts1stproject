<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<link rel="stylesheet" type="text/css" href="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/customer">
	<div id="pertama">
	<span class="h2-judul">Create Customer</span>
	<!-- #####1. Personal Information############################################################### -->
		<table border="1" class="input">
			<tr>
				<th colspan="2">1. Personal Information</th>
			</tr>
			<tr>
				<td class="inlabel">Customer Name</td>
				<td><html:text property="custName" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">Customer Full Name</td>
				<td><html:text property="custFullName" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">ID Type</td>
				<td>
				<html:select property="custIdType">
					<html:option value=""></html:option>
					<html:option value="1-KTP">1-KTP</html:option>
					<html:option value="2-SIM">2-SIM</html:option>
					<html:option value="3-Passport">3-Passport</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">ID No.</td>
				<td><html:text property="custIdNumber" maxlength="40" styleClass="sedang"/></td>
			</tr>
			<tr>
				<td class="inlabel">ID Expiry Date</td>
				<td>
				<html:text property="custIdExp" styleId="idExp" styleClass="kecil" maxlength="10"/><span>&nbsp;(DD/MM/YYYY)</span>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Gender</td>
				<td>
				<html:select property="custGender">
					<html:option value=""></html:option>
					<html:option value="0-MALE">0-MALE</html:option>
					<html:option value="1-FEMALE">1-FEMALE</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Place of Birth</td>
				<td><html:text property="custPlaceOfBirth" styleClass="sedang" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">City of Birth</td>
				<td> <html:hidden property="custCityOfBirth" styleId="valciofb"/>
				<input type="button" class="in" value="..." onclick="windowCity()"/><span id="valciofb2"></span>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Province of Birth</td>
				<td> 
				<html:hidden property="custProvinceOfBirth" styleId="valprob"/>
				<input type="button" class="in" value="..." onclick="windowProvince()"/><span id="valprob2"></span>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Date of Birth</td>
				<td>
				<html:text property="custDateOfBirth" styleId="dob" styleClass="kecil" maxlength="10"/><span>&nbsp;(DD/MM/YYYY)</span>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Age</td>
				<td>
				<input type="text" class="kecil" id="year" disabled="disabled"/>&nbsp;year(s)&nbsp;
				<input type="text" class="kecil" id="month" disabled="disabled"/>&nbsp;month(s)
				</td>
			</tr>
			<tr>
				<td class="inlabel">Marital Status</td>
				<td>
				<html:select property="custMaritalStatus" styleId="custMaritalStatus">
					<html:option value=""></html:option>
					<html:option value="01-Single">01-Single</html:option>
					<html:option value="02-Married">02-Married</html:option>
					<html:option value="03-Widowed">03-Widowed</html:option>
					<html:option value="04-Divorced">04-Divorced</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Number of Dependents</td>
				<td><html:text property="custNumOfDependents" styleClass="kecil"/></td>
			</tr>
			<tr>
				<td class="inlabel">Level of Education</td>
				<td>
				<html:select property="custLvlOfEducation">
					<html:option value=""></html:option>
					<html:option value="01-SD">01-SD</html:option>
					<html:option value="02-SMP">02-SMP</html:option>
					<html:option value="03-SMA">03-SMA</html:option>
					<html:option value="04-D3">04-D3</html:option>
					<html:option value="05-S1">05-S1</html:option>
					<html:option value="06-S2">06-S2</html:option>
					<html:option value="07-S3">07-S3</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Mother's Maiden Name</td>
				<td><html:text property="custMotherMaiden"/></td>
			</tr>
			<tr class="none spouse">
				<td colspan="2" class="th">Spouse Information</td>
			</tr>
			<tr class="none spouse">
				<td class="inlabel">Spouse Name</td>
				<td><html:text property="custSpouseName" styleClass="sedang"></html:text> </td>
			</tr>
			<tr class="none spouse">
				<td class="inlabel">ID No.</td>
				<td><html:text property="custSpouseId" styleClass="sedang"></html:text> </td>
			</tr>
			<tr class="none spouse">
				<td class="inlabel">Date of Birth</td>
				<td>
				<html:text property="custSpouseDob" styleId="spDob" styleClass="kecil" maxlength="10"/><span>&nbsp;(DD/MM/YYYY)</span>
				</td>
			</tr>
			<tr class="none spouse">
				<td class="inlabel">Age</td>
				<td>
				<input type="text" class="kecil" id="year" disabled="disabled"/>&nbsp;year(s)&nbsp;
				<input type="text" class="kecil" id="month" disabled="disabled"/>&nbsp;month(s)
				</td>
			</tr>
			<tr class="none spouse">
				<td class="inlabel">Penuptial Agreement</td>
				<td>
				<html:select property="custSpousePA">
					<html:option value=""></html:option>
					<html:option value="Yes">Yes</html:option>
					<html:option value="No">No</html:option>
				</html:select> 
				</td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="Next >>" id="nextForm1"/> &nbsp; 
				<a href="javascript:backCustomer('customer.do?task=showCustomer');"><input type="button" value="Cancel" /></a>
				</td>
			</tr>
		</table>
		</div>
		<!-- ######## 2. Residence Information ###################################################### -->
		<div class="none" id="kedua">
		<span class="h2-judul">Create Customer</span>
		<table border="1" class="input">
			<tr>
				<th colspan="2">2. Residence Information</th>
			</tr>
			<tr>
				<td class="inlabel">Mailing Address</td>
				<td>
				<html:select property="mailingAddress">
					<html:option value=""></html:option>
					<html:option value="0-Officer">0-Officer</html:option>
					<html:option value="R-Residence">R-Residence</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td rowspan="4" class="inlabel">Residence Address</td>
				<td><html:text property="custAddress1" maxlength="40"/></td>
			</tr>
			<tr>
				<td><html:text property="custAddress2" maxlength="40"/></td>
			</tr>
			<tr>
				<td><html:text property="custAddress3" maxlength="40"/></td>
			</tr>
			<tr>
				<td><html:text property="custAddress4" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">City</td>
				<td> <html:hidden property="custAdrsCity"/>
				<input type="button" class="in" value="..." onclick="windowCity()"/><span id="city"></span>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Province</td>
				<td> <html:hidden property="custAdrsProvince"/>
				<input type="button" class="in" value="..." onclick="windowProvince()"/><span id="prob"></span>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Zip Code</td>
				<td>
				<html:text property="custAdrsZipCode" styleClass="kecil" maxlength="10"/>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Kelurahan</td>
				<td>
				<html:text property="custAdrsKelurahan" maxlength="40"/>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Kecamatan</td>
				<td>
				<html:text property="custAdrsKecamatan" maxlength="40"/>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Home Phone No.</td>
				<td><html:text property="custAdrsHomePhone" styleClass="sedang" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">Mobile Phone No.</td>
				<td><html:text property="custAdrsMobilePhone" styleClass="sedang" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">Email Address</td>
				<td><html:text property="custAdrsEmail" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">Residence Status</td>
				<td>
				<html:select property="custAdrsResStatus">
					<html:option value=""></html:option>
					<html:option value="00-Owner">00-Owner</html:option>
					<html:option value="01-Parent">01-Parent</html:option>
					<html:option value="02-Rent">02-Rent</html:option>
					<html:option value="03-Relatives">03-Relatives</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Length of Stay</td>
				<td>
				<html:text property="custAdrsLos" styleClass="kecil" maxlength="40"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="<< Prev" id="prevForm2"/> &nbsp; 
				<input type="button" value="Next >>" onclick="nextForm2()"/> &nbsp; 
				<a href="javascript:backCustomer('customer.do?task=showCustomer');"><input type="button" value="Cancel" /></a>
				</td>
			</tr>
		</table>
		</div>
		<html:hidden property="task"/>
		</html:form>
		<!-- <a href="#" class="eng">coba</a> -->
		
		<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
		<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
		<script src="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.js"></script>
		<script type="text/javascript">
		$("#idExp").datetimepicker({ lang:'en', timepicker:false, format:'d/m/Y', closeOnDateSelect:true});
		$("#dob").datetimepicker({ startDate:'1994/05/25', lang:'en', timepicker:false, format:'d/m/Y', closeOnDateSelect:true});
		$("#idExp,#dob").keypress(function(e){ e.preventDefault(); });
		$("#idExp,#dob").attr('autocomplete', 'off');
		$("#custMaritalStatus").val($("#custMaritalStatus option:first").val());
		$("#dob").on("change", function() {
            var dob = new Date(this.value);
            var today = new Date();
            var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
            $("#year").val("");
            $("#month").val("");
        });
		$("#custMaritalStatus").on("change", function() {
			var val =  this.value;
			  if(val == "02-Married"){
				  $(".spouse").show();
			  }else{
				  $(".spouse").hide();
			  }
		});
		$("#nextForm1").click(function(){
		    $("#pertama").hide(); 
		    $("#kedua").show(); 
		});
		$("#prevForm2").click(function(){
		    $("#pertama").show(); 
		    $("#kedua").hide(); 
		});
		function nextForm2(){
			document.forms[0].task.value="doAdd";
			document.forms[0].submit();
		}
		function backCustomer(src) {
			parent.content.location = src;
		}
		var params = 'scrollbars=0,resizable=0,status=0,location=0,toolbar=0,menubar=0,width=620,height=530,left=350,top=70';
		function windowProvince() {
		    window.open('province.do?task=windowProvince','Province Window',params,false);   // it takes lotsof more arguments you can use as per your needs
		}
		function windowCity() {
			/* var val = document.getElementById("valciofb").value();
			alert(val); */
		    window.open('city.do?task=windowCity','City Window',params,false);   // it takes lotsof more arguments you can use as per your needs
		}
		
	
	</script>
</body>
</html>