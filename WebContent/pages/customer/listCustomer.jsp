<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<link rel="stylesheet" type="text/css" href="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/customer">
	<span class="h2-judul">Customer Information</span>
	<div class="alert alert-success center none"><i class="fa fa-check-circle-o fa-fw"></i>Pesan</div>
	<table border="1" class="input">
			<tr>
				<th colspan="2" class="top">Enter Search Criteria</th>
			</tr>
			<tr>
				<td class="inlabel">Customer Name</td>
				<td><html:text property="searchCustName"/></td>
			</tr>
			<tr>
				<td class="inlabel">ID No.</td>
				<td><html:text property="searchCustIdNumber"/></td>
			</tr>
			<tr>
				<td class="inlabel">Date of Birth</td>
				<td><html:text property="searchCustDateOfBirth" styleClass="kecil" styleId="dob"/>&nbsp;(DD/MM/YYYY)</td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="Refresh" onclick="doRefresh()"/> &nbsp;<input type="button" value="Search" onclick="searchPage()"/>
				</td>
				
			</tr>
			<html:hidden property="task"/>
		</table>
		</html:form>
		<table border="1" class="view">
			<tr>
				<th colspan="5" class="top">Customer List</th>
			</tr>
			<tr>
				<th >Customer Name</th>
				<th >Address</th>
				<th >ID Type</th>
				<th >ID No.</th>
				<th >Birth Date</th>
			</tr>
			<logic:empty name="customerForm" property="listCustomer">
			<tr>
				<td colspan="5" class="center blue">Data is not found</td>
			</tr>
			</logic:empty>
			<logic:notEmpty name="customerForm" property="listCustomer">
			<logic:iterate id="idlist" name="customerForm" property="listCustomer">
				<tr>
					<td><a href="javascript:openDetail('<bean:write name="idlist" property="custId"/>')"><bean:write name="idlist" property="custFullName" /></a></td>
					<td><bean:write name="idlist" property="custName" /></td>
					<td><bean:write name="idlist" property="custIdType" /></td>
					<td><bean:write name="idlist" property="custIdNumber" /></td>
					<td><bean:write name="idlist" property="custDateOfBirth" /></td>
				</tr>
			</logic:iterate>
			<tr>
				<td colspan="5" class="infoot">
				<input type="button" class="nav left" value="<<"/> &nbsp; 
				<input type="button" class="nav left" value="<"/> &nbsp; 
				<input type="button" class="nav left" value=">"/> &nbsp; 
				<input type="button" class="nav left" value=">>"/> &nbsp;
				<span class="search left">&nbsp;&nbsp;&nbsp;Page&nbsp;</span>
				<input type="text" class="search left"/>
				<input type="button" class="nav left" value="Go"/> &nbsp;
				<span class="search right">&nbsp;&nbsp;&nbsp;Total 1000 Record(s), Page 1 of 2&nbsp;</span>
			</tr>
			</logic:notEmpty>
			<tr>
				<td colspan="5" class="infoot">
				<a href="javascript:addPage('customer.do?task=showAdd');"><input type="button" value="Add" /></a>
				</td>
			</tr>
		</table>
		<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
		<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
		<script src="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.js"></script>
		<script type="text/javascript">
		$("#dob").datetimepicker({ startDate:'1994/05/25', lang:'en', timepicker:false, format:'d/m/Y', closeOnDateSelect:true});
		//$("#idExp,#dob").keypress(function(e){ e.preventDefault(); });
		$("#dob").attr('autocomplete', 'off');
		function doRefresh(){
			document.forms[0].action="customer.do";
			document.forms[0].task.value="showCustomer";
			document.forms[0].submit();
		}
		function searchPage(){
			document.forms[0].task.value="doSearch";
			document.forms[0].submit();
		}
		function addPage(src) {
			parent.content.location = src;
		}
		
		</script>
</body>
</html>