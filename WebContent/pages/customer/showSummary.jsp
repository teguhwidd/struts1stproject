<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<link rel="stylesheet" type="text/css" href="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/customer">
		<!-- ################ Customer Summary ####1. Personal Information################################ -->
		
		<span class="h2-judul">Customer Summary</span>
		<table border="1" class="input">
			<tr>
				<th colspan="4">1. Personal Information</th>
			</tr>
			<tr>
				<td class="inlabel">Customer Name</td>
				<td colspan="3"><b><bean:write name="customerForm" property="custName"/></b></td>
			</tr>
			<tr>
				<td class="inlabel">Customer Full Name</td>
				<td colspan="3"><b><bean:write name="customerForm" property="custFullName"/></b></td>
			</tr>
			<tr>
				<td class="inlabel">ID Type</td>
				<td>
				<b><bean:write name="customerForm" property="custIdType"/></b>
				</td>
				<td class="inlabel">ID No.</td>
				<td>
				<b><bean:write name="customerForm" property="custIdNumber"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">ID Expiry Date</td>
				<td colspan="3">
				<b><bean:write name="customerForm" property="custIdExp"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Gender</td>
				<td colspan="3">
				<b><bean:write name="customerForm" property="custGender"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Place of Birth</td>
				<td>
				<b><bean:write name="customerForm" property="custPlaceOfBirth"/></b>
				</td>
				<td class="inlabel">City of Birth</td>
				<td><b>-</b>
				<b><bean:write name="customerForm" property="custCityOfBirth"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Province of Birth</td>
				<td colspan="3"><b>-</b>
				<b><bean:write name="customerForm" property="custProvinceOfBirth"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Date of Birth</td>
				<td>
				<b><bean:write name="customerForm" property="custDateOfBirth"/></b>
				</td>
				<td class="inlabel">Age</td>
				<td>
				<b>-</b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Marital Status</td>
				<td>
				<b><bean:write name="customerForm" property="custMaritalStatus"/></b>
				</td>
				<td class="inlabel">Number of Dependents</td>
				<td>
				<b><bean:write name="customerForm" property="custNumOfDependents"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Mother's Maiden Name</td>
				<td>
				<b><bean:write name="customerForm" property="custMotherMaiden"/></b>
				</td>
				<td class="inlabel">Level of Education</td>
				<td>
				<b><bean:write name="customerForm" property="custLvlOfEducation"/></b>
				</td>
			</tr>
			<!-- ################ Customer Summary ####2. Residence Information################################ -->
			<tr>
				<th colspan="4">2. Residence Information</th>
			</tr>
			<tr>
				<td class="inlabel">Mailing Address</td>
				<td colspan="3">
				<b><bean:write name="customerForm" property="mailingAddress"/></b>
				</td>
			</tr>
			<tr>
				<td rowspan="4" class="inlabel">Residence Address</td>
				<td colspan="3">
				<b><bean:write name="customerForm" property="custAddress1"/></b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b><bean:write name="customerForm" property="custAddress2"/></b></td>
			</tr>
			<tr>
				<td colspan="3"><b><bean:write name="customerForm" property="custAddress3"/></b></td>
			</tr>
			<tr>
				<td colspan="3"><b><bean:write name="customerForm" property="custAddress4"/></b></td>
			</tr>
			<tr>
				<td class="inlabel">City</td>
				<td><b>-</b>
				<b><bean:write name="customerForm" property="custAdrsCity"/></b>
				</td>
				<td class="inlabel">Province</td>
				<td><b>-</b>
				<b><bean:write name="customerForm" property="custAdrsProvince"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Zip Code</td>
				<td colspan="3">
				<b><bean:write name="customerForm" property="custAdrsZipCode"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Kelurahan</td>
				<td>
				<b><bean:write name="customerForm" property="custAdrsKelurahan"/></b>
				</td>
				<td class="inlabel">Kecamatan</td>
				<td>
				<b><bean:write name="customerForm" property="custAdrsKecamatan"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Home Phone No.</td>
				<td><b><bean:write name="customerForm" property="custAdrsHomePhone"/></b></td>
				<td class="inlabel">Mobile Phone No.</td>
				<td><b><bean:write name="customerForm" property="custAdrsMobilePhone"/></b></td>
			</tr>
			<tr>
				<td class="inlabel">Email Address</td>
				<td><b><bean:write name="customerForm" property="custAdrsEmail"/></b></td>
				<td class="inlabel">Residence Status</td>
				<td>
				<b><bean:write name="customerForm" property="custAdrsResStatus"/></b>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Length of Stay</td>
				<td colspan="3">
				<b><bean:write name="customerForm" property="custAdrsLos"/></b>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="infoot">
				<input type="button" value="Edit"/> &nbsp; 
				<a href="javascript:backCustomer('customer.do?task=showCustomer');"><input type="button" value="Cancel" /></a>
				</td>
			</tr>
		</table>
		<br />
		<table border="1" class="view">
			<tr>
				<th colspan="5" class="top">Customer Application List</th>
			</tr>
			<tr>
				<th >Ref No.</th>
				<th >Date Created</th>
				<th >Creator</th>
				<th >Status</th>
				<th >Hold By</th>
			</tr>
			<logic:empty name="customerForm" property="listCustomer">
			<tr>
				<td colspan="5" class="center blue">Data is not found</td>
			</tr>
			</logic:empty>
			<logic:notEmpty name="customerForm" property="listCustomer">
			<logic:iterate id="idlist" name="customerForm" property="listCustomer">
				<tr>
					<td><a href="javascript:openDetail('<bean:write name="idlist" property="refNo"/>')"><bean:write name="idlist" property="refNo" /></a></td>
					<td><bean:write name="idlist" property="dateReceived" /></td>
					<td><bean:write name="idlist" property="holdBy" /></td>
					<td><bean:write name="idlist" property="status" /></td>
					<td><bean:write name="idlist" property="holdBy" /></td>
				</tr>
			</logic:iterate>
			<tr>
				<td colspan="5" class="infoot">
				<input type="button" class="nav left" value="<<"/> &nbsp; 
				<input type="button" class="nav left" value="<"/> &nbsp; 
				<input type="button" class="nav left" value=">"/> &nbsp; 
				<input type="button" class="nav left" value=">>"/> &nbsp;
				<span class="search left">&nbsp;&nbsp;&nbsp;Page&nbsp;</span>
				<input type="text" class="search left"/>
				<input type="button" class="nav left" value="Go"/> &nbsp;
				<span class="search right">&nbsp;&nbsp;&nbsp;Total 1000 Record(s), Page 1 of 2&nbsp;</span>
			</tr>
			</logic:notEmpty>
			<tr>
				<td colspan="5" class="infoot">
				<button onclick="addPage('<bean:write name="customerForm" property="custId"/>')">Add</button>
				</td>
			</tr>
		</table>
		<html:hidden property="task"/>
		<html:hidden property="custId"/>
		</html:form>
		<!-- <a href="#" class="eng">coba</a> -->
		
		<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
		<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
		<script src="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.js"></script>
		<script type="text/javascript">
		$("#idExp").datetimepicker({ lang:'en', timepicker:false, format:'d/m/Y', closeOnDateSelect:true});
		$("#dob").datetimepicker({ startDate:'1994/05/25', lang:'en', timepicker:false, format:'d/m/Y', closeOnDateSelect:true});
		$("#idExp,#dob").keypress(function(e){ e.preventDefault(); });
		$("#idExp,#dob").attr('autocomplete', 'off');
		$("#dob").on("change", function() {
            var dob = new Date(this.value);
            var today = new Date();
            var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
            $("#year").val("");
            $("#month").val("");
        });
		$("#custMaritalStatus").on("change", function() {
			var val =  this.value;
			  if(val == "02-Married"){
				  $(".spouse").show();
			  }else{
				  $(".spouse").hide();
			  }
		});
		$("#nextForm1").click(function(){
		    $("#pertama").hide(); 
		    $("#kedua").show(); 
		});
		$("#prevForm2").click(function(){
		    $("#pertama").show(); 
		    $("#kedua").hide(); 
		});
		function addCustomer(){
			document.forms[0].task.value="doAdd";
			document.forms[0].submit();
		}
		function nextForm2(){
			document.forms[0].task.value="doAdd";
			document.forms[0].submit();
		}
		function addPage(src) {
			parent.content.location = src;
		}
		function addPage(code){
			document.forms[0].task.value = "showAdd";
			document.forms[0].custId.value = code;
			document.forms[0].action="application.do";
			document.forms[0].submit();
		}
		function backCustomer(src) {
			parent.content.location = src;
		}
		var params = 'scrollbars=0,resizable=0,status=0,location=0,toolbar=0,menubar=0,width=620,height=530,left=350,top=70';
		function windowProvince() {
		    window.open('province.do?task=windowProvince','Province Window',params,false);   // it takes lotsof more arguments you can use as per your needs
		}
		function windowCity() {
		    window.open('city.do?task=windowCity','City Window',params,false);   // it takes lotsof more arguments you can use as per your needs
		}
	
	</script>
</body>
</html>