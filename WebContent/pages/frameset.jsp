<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<!DOCTYPE>
<html>
	<head> 
	<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
	
	<title></title>
	</head>
	<frameset rows="6%,*,4%" border="0">
		<frame name="header" scrolling="no" src="login.do?task=showHeader"/>
		<frameset cols="15%,*">
			<frame name="menu" scrolling="no" src="login.do?task=showMenu" />
			<frame id="content" name="content" src="login.do?task=showContent" />
		</frameset>
		<frame name="footer" scrolling="no" src="login.do?task=showFooter"/>
	
	</frameset>
	
</html>
