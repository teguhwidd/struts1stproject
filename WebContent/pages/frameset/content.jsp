<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String userName = " " + (String) session.getAttribute("userName");
	String userRole = " " + (String) session.getAttribute("userRole");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>BCA</title>
<style>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
</style>
</head>
<body>
	<h1> CCOS-BCA : Consumer Credit Origination System - Bank Central Asia </h1>
	<p> <font face="Courier" > Welcome <b><%=userName%></b></font></p>
	<p> <font face="Courier" > You are currently logged as <b><%=userRole%></b>.</font></p>
</body>
</html>