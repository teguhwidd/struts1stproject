<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String userName = " " + (String) session.getAttribute("userName");
	String userRole = " " + (String) session.getAttribute("userRole");
%>
<!DOCTYPE>
<html>
	<head> 
	<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
	<title></title>
	</head>
	<body bgcolor="#006699">
	<span class="header-frame left">CCOS-BCA Personal Loan</span> 
	<span class="header-frame2 right" id="time"></span>
	<span class="header-frame2 right">Welcome <%=userName%>,&nbsp;&nbsp;</span> 
	</body>
	<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
	<script>
	$(window).on('load', function() {
	    startTime();
	});
	function startTime() {
		var today = new Date();
	    var hours = today.getHours();
	    var ampm = hours >= 12 ? 'PM' : 'AM'; //logic if ? true:false
	    var minutes = today.getMinutes();
	    var seconds = today.getSeconds();
	    hours = hours % 12; //modulus (sisa hasil bagi, 23%12=11)
	    hours = checkTime(hours);
	    minutes = checkTime(minutes);
	    seconds = checkTime(seconds);
	    document.getElementById('time').innerHTML =
	    hours+":"+minutes+":"+seconds+" "+ampm;
	    var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
	    if (i < 10) {i = "0" + i};  // add 0 didepan angka < 10
	    return i;
	}
	
	</script>
</html>