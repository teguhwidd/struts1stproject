<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%
	String userName = (String) session.getAttribute("userName");
	String userRole = (String) session.getAttribute("userRole");
	String userId = (String) session.getAttribute("userId");
%>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<title></title>
<style>
html, body { padding: 0; margin: 0; }
body{
/* border-right: 0.3px solid black; */
}
</style>
</head>
<body bgcolor="#e6ebf4"><!-- 
 --><body bgcolor="#e6e6e6">

	<table border="1" class="menu">
<!-- 	<table cellspacing="0" cellpadding="5" border=1 class="menu"> -->
		<tr>
			<th>User ID : <%=userId%></th>
		</tr>
		<tr>
			<td class="top">CCOS-BCA</td>
		</tr>
		<% if ("admin".equals(userRole)) { %>
		<tr>
			<td>
				<a href="javascript:showPage('customer.do?task=showHome');" class="ano">:: Home</a>
			</td>
		</tr>
		<tr>
			<td id="main"><a href="#" class="ano">:: Maintenance</a></td>
		</tr>
		<tr id="show1" class="none">
			<td><a href="javascript:showPage('province.do?task=showProvince');" class="ano">&nbsp;&nbsp;&nbsp;:: Province</a></td>
		</tr>
		<tr id="show2" class="none">
			<td><a href="javascript:showPage('city.do?task=showCity');" class="ano">&nbsp;&nbsp;&nbsp;:: City</a></td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('user.do?task=showUser');" class="ano">:: User</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="login.do?task=doLogout" target="_top" class="ano">:: Logout</a>
			</td>
		</tr>
		<% } else { %>
		<tr>
			<td>
				<a href="javascript:showPage('customer.do?task=showHome');" class="ano">:: Home</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('customer.do?task=showCustomer');" class="ano">:: New Application</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('report.do?task=showReport');" class="ano">:: Report</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('report.do?task=showReport2');" class="ano">:: Report2</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="login.do?task=doLogout" target="_top" class="ano">:: Logout</a>
			</td>
		</tr>
		<% } %>
		<!-- 
		<tr>
			<td>######batas admin#####</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('customer.do?task=showHome');" class="ano">:: Home</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('customer.do?task=showCustomer');" class="ano">:: New Application</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('report.do?task=showReport');" class="ano">:: Report</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="login.do?task=doLogout" target="_top" class="ano">:: Logout</a>
			</td>
		</tr>
		<tr>
			<td>=====================</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:showPage('customer.do?task=showCustomer');" class="ano">:: Customer</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:addPage('customer.do?task=showAdd');" class="ano">:: Add Customer</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:addPage('province.do?task=showAdd');" class="ano">:: Add Povince</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:addPage('city.do?task=showAdd');" class="ano">:: Add City</a>
			</td>
		</tr>
		
		<tr>
			<td>
				<a href="javascript:addPage('user.do?task=showAdd');" class="ano">:: Add User</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:addPage('application.do?task=showAdd');" class="ano">:: Add Application</a>
			</td>
		</tr>
		<tr>
			<td>=====================</td>
		</tr>  -->
		
	</table>
</body>
<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
<script>
	function goHome(src) {
		parent.content.location = src;
	}
	function showPage(src) {
		parent.content.location = src;
	}
	function addPage(src) {
		parent.content.location = src;
	}
	$("#main").click(function(){
	    $("#show1,#show2").slideToggle("fast"); 
	    /* $("#show").toggle(); */
	    /* $("#show").toggle(500); */
	});
</script>
</html>