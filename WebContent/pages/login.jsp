<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" href="resources/etc/bootstrap-3.3.7/dist/css/bootstrap.css"> -->
<link rel="stylesheet" href="resources/css/main.css" />
<style>
body  { 
    background-image: url('resources/assets/login_bg.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center; 
    /* background-size: 800px auto; */
}
</style>
<title>Login Page</title>
</head>
<body class=login>
	<!-- <img src="resources/assets/login_bg.jpg" alt="" /> -->
	<div class="login">
		<html:form action="/login" styleId="formLogin">
		<table border="" class="login">
			<tr>
				<td>User ID</td>
				<td><html:text property="userId" styleClass="login" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><html:password property="userPassword" styleClass="login"/></td>
			</tr>
			<tr>
				<td colspan="2">
				<input type="submit" value="Login" class="right"/>
				<!-- <input type="button" onclick="login()" value="Login" class="right"/> -->
				<!-- submit v2, with js function onclick=lgin() -->
				</td>
			</tr>
		</table>
		<html:hidden property="task" value="doLogin"/>
	</html:form>
	</div>
	<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
	<!-- <script src="resources/etc/jquery/jquery-validate.bootstrap-tooltip.js"></script> -->
	<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
	<script>
		var i1 = "User ID";
		$(document).ready(function () {
		    $('#formLogin').validate({
		    	rules: {
					userId: {required: true},
	            },
	            messages: {
	            	/* userId: {required: i1+" harus diisi !!!"}, */
	            },
	            /* tooltip_options: {
	            	coba: {placement:'right', html:true}
            	}, */
	            submitHandler: function(form) { form.submit(); }
		    });
		});
		/* function login(){
			document.forms[0].task.value="doLogin";
			alert( "Valid: " + form.valid() );
			document.forms[0].submit();
		} */
	</script>
</body>
</html>