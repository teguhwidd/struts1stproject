<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/province" styleId="formProvince">
		<table border="1" class="input">
			<tr>
				<th colspan="2">Province Information</th>
			</tr>
			<tr>
				<td class="inlabel">Provine ID</td>
				<td><html:text property="provinceId" maxlength="10" value=""/></td>
			</tr>
			<tr>
				<td class="inlabel">Province Name</td>
				<td><html:text property="provinceName" maxlength="40" value=""/></td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<html:hidden property="task" value="doAdd"/>
				<input type="submit" value="Save"/> &nbsp; 
				<a href="javascript:backPage('province.do?task=showProvince');"><input type="button" value="Back" /></a>
				</td>
			</tr>
		</table>
		</html:form>
		<!-- <a href="#" class="eng">coba</a> -->
		
	<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
	<!-- <script src="resources/etc/jquery/jquery-validate.bootstrap-tooltip.js"></script> -->
	<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
	<script>
		var i1 = "Provice ID";
		var i2 = "Province Name";
		$(document).ready(function () {
			$("input[name=provinceId]").focus();
		    $('#formProvince').validate({
		    	rules: {
		    		provinceId: {required: true, digits: true},
		    		provinceName: {required: true}
	            },
	            messages: {
	            	provinceId: {required: i1+" harus diisi !!!", digits: i1+" hanya boleh diisi angka !!!"},
	            	provinceName: {required: i2+" harus diisi !!!"}
	            },
	            /* tooltip_options: {
	            	coba: {placement:'right', html:true}
            	}, */
	            submitHandler: function(form) { form.submit(); }
		    });
		});
		function backPage(src) {
			parent.content.location = src;
		}
	
	</script>
</body>
</html>