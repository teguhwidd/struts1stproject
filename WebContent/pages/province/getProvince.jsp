<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<style>
td.click{
	cursor: pointer;
}
</style>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/province">
	<table border="1" class="input">
			<tr>
				<th colspan="2" class="top">Enter Search Criteria</th>
			</tr>
			<tr>
				<td rowspan="2" class="inlabel">Search By</td>
				<td rowspan="2">
				<input type="radio" name="search" value="1"/>Code <br />
				<input type="radio" name="search" value="1"/>Description
				</td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td class="inlabel">Enter Value</td>
				<td><html:text property="searchProvinceName" value=""/></td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="Search" onclick="searchPage()"/> 
				</td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="Clear" onclick=""/> 
				<input type="button" value="Close" onclick=""/> 
				</td>
			</tr>
		</table>
		<table border="1" class="view">
			<tr>
				<th colspan="2" class="top">Province List</th>
			</tr>
			<tr>
				<th >Code</th>
				<th >Description</th>
			</tr>
			<%-- <logic:empty name="provinceForm" property="listProvince">
			<tr>
				<td colspan="2" class="center blue">Data is not found</td>
			</tr>
			</logic:empty> --%>
			<logic:notEmpty name="provinceForm" property="listProvince">
			<logic:iterate id="idlist" name="provinceForm" property="listProvince">
				<tr>
					<td class="click" onclick="pick('<bean:write name="idlist" property="provinceId" />','<bean:write name="idlist" property="provinceName" />')">
						<bean:write name="idlist" property="provinceId" />
					</td>
					<td class="click" onclick="pick('<bean:write name="idlist" property="provinceId" />','<bean:write name="idlist" property="provinceName" />')">
						<bean:write name="idlist" property="provinceName" />
					</td>
				</tr>
			</logic:iterate>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" class="nav left" value="<<"/> &nbsp; 
				<input type="button" class="nav left" value="<"/> &nbsp; 
				<input type="button" class="nav left" value=">"/> &nbsp; 
				<input type="button" class="nav left" value=">>"/> &nbsp;
				<span class="search left">&nbsp;&nbsp;&nbsp;Page&nbsp;</span>
				<input type="text" class="search left"/>
				<input type="button" class="nav left" value="Go"/> &nbsp;
				<span class="search right">&nbsp;&nbsp;&nbsp;Total 1000 Record(s), Page 1 of 2&nbsp;</span>
			</tr>
			</logic:notEmpty>
		</table>
		<html:hidden property="task"/>
		<html:hidden property="field"/>
		</html:form>
		<script>
		function actionPage(src) {
			parent.content.location = src;
		}
		function searchPage(){
			document.forms[0].task.value="doGetSearch";
			document.forms[0].submit();
		}
		function pick(prid,prname){
			var gabungpr = ' '+prid+' - '+prname;
			window.opener.document.getElementById("valprob").value = prid;
			window.opener.document.getElementById("valprob2").innerHTML = gabungpr;
			window.opener.document.getElementById("valciofb").value = prid;
			window.opener.document.getElementById("valciofb2").innerHTML = "";
			window.close();
		}
		</script>
</body>
</html>