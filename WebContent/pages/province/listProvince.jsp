<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/province">
	<table border="1" class="input">
			<tr>
				<th colspan="2" class="top">Enter Search Criteria</th>
			</tr>
			<tr>
				<td class="inlabel">Province Name</td>
				<td><html:text property="searchProvinceName"/></td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="Refresh" onclick="doRefresh()"/> &nbsp;<input type="button" value="Search" onclick="searchPage()"/> 
				</td>
				
			</tr>
		</table>
		<table border="1" class="view">
			<tr>
				<th colspan="3" class="top">Province List</th>
			</tr>
			<tr>
				<th >Province ID</th>
				<th >Province Name</th>
				<th >Opsi</th>
			</tr>
			<logic:empty name="provinceForm" property="listProvince">
			<tr>
				<td colspan="2" class="center blue">Data is not found</td>
			</tr>
			</logic:empty>
			<logic:notEmpty name="provinceForm" property="listProvince">
			<logic:iterate id="idlist" name="provinceForm" property="listProvince">
				<tr>
					<%-- <td><a href="javascript:openDetail('<bean:write name="idlist" property="provinceId"/>')"><bean:write name="idlist" property="provinceId" /></a></td> --%>
					<td><bean:write name="idlist" property="provinceId" /></td>
					<td><bean:write name="idlist" property="provinceName" /></td>
					<td class="">
					<button class="btn bblue" onclick="showEdit('<bean:write name="idlist" property="provinceId"/>')">Edit</button>
					<button class="btn bred" onclick="showDelete('<bean:write name="idlist" property="provinceId"/>')">Delete</button>
					</td>
				</tr>
			</logic:iterate>
			<tr>
				<td colspan="3" class="infoot">
				<input type="button" class="nav left" value="<<"/> &nbsp; 
				<input type="button" class="nav left" value="<"/> &nbsp; 
				<input type="button" class="nav left" value=">"/> &nbsp; 
				<input type="button" class="nav left" value=">>"/> &nbsp;
				<span class="search left">&nbsp;&nbsp;&nbsp;Page&nbsp;</span>
				<input type="text" class="search left"/>
				<input type="button" class="nav left" value="Go"/> &nbsp;
				<span class="search right">&nbsp;&nbsp;&nbsp;Total 1000 Record(s), Page 1 of 2&nbsp;</span>
			</tr>
			</logic:notEmpty>
			<tr>
				<td colspan="3" class="infoot">
				<a href="javascript:actionPage('province.do?task=showAdd');"><input type="button" value="Add" /></a>
				</td>
			</tr>
		</table>
		<html:hidden property="task"/>
		<html:hidden property="provinceId"/>
		</html:form>
		<script>
		function doRefresh(){
			document.forms[0].action="province.do";
			document.forms[0].task.value="showProvince";
			document.forms[0].submit();
		}
		function actionPage(src) {
			parent.content.location = src;
		}
		function showEdit(code) {
			document.forms[0].task.value = "showEdit";
			document.forms[0].provinceId.value = code;
			document.forms[0].submit();
		}
		function showDelete(code){
			document.forms[0].task.value = "doDelete";
			document.forms[0].provinceId.value = code;
			document.forms[0].submit();
		}
		function searchPage(){
			document.forms[0].task.value="doSearch";
			document.forms[0].submit();
		}
		</script>
</body>
</html>