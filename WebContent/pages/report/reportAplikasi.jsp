<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<link rel="stylesheet" type="text/css" href="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="report" styleId="formReport">
	<table class="input" >
		<tr>
			<th>Report List</th>
		</tr>
		<tr>
			<td> <html:radio property="radioLaporanApp" value="yes" >Laporan Jumlah Aplikasi per Cabang</html:radio> </td>
		</tr>
		<tr>
			<td class="infoot">Month :&nbsp;
			<html:select property="month" styleClass="report">
				<html:option value="01">Jan</html:option>
				<html:option value="02">Feb</html:option>
				<html:option value="03">Mar</html:option>
				<html:option value="04">Apr</html:option>
				<html:option value="05">May</html:option>
				<html:option value="06">Jun</html:option>
				<html:option value="07">Jul</html:option>
				<html:option value="08">Agu</html:option>
				<html:option value="09">Sep</html:option>
				<html:option value="10">Oct</html:option>
				<html:option value="11">Nov</html:option>
				<html:option value="12">Dec</html:option>
			</html:select>&nbsp; Year : &nbsp;
			<html:text property="year" styleClass="report" maxlength="4"></html:text>
			<html:hidden property="task" value="generateReport"/>
			<a href=""><input type="submit" value="Generate"/></a>
			</td>
		</tr>
	</table>
	
	</html:form>
</body>
</html>