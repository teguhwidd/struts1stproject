<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<link rel="stylesheet" type="text/css" href="resources/etc/jquery/jquery-datetimepicker-2.5.12/jquery.datetimepicker.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/user" styleId="formUser">
		<table border="1" class="input">
			<tr>
				<th colspan="2">User Information</th>
			</tr>
			<tr>
				<td class="inlabel">User ID</td>
				<td><html:text property="userId" value="" maxlength="8"/></td>
			</tr>
			<tr>
				<td class="inlabel">User Name</td>
				<td><html:text property="userName" value="" maxlength="40"/></td>
			</tr>
			<tr>
				<td class="inlabel">Role</td>
				<td>
				<html:select property="userRole" styleId="role">
					<html:option value=""></html:option>
					<html:option value="admin">1 - Admin</html:option>
					<html:option value="operator">2. Operator</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td class="inlabel">Password</td>
				<td><html:password property="userPassword" value="" maxlength="20"/></td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<html:hidden property="task" value="doAdd"/>
				<input type="submit" value="Save"/> &nbsp; 
				<a href="javascript:backUser('user.do?task=showUser')"><input type="button" value="Back" /></a>
				</td>
			</tr>
		</table>
		</html:form>
		<!-- <a href="#" class="eng">coba</a> -->
		
		<script src="resources/js/jquery-3.1.1.js" type="text/javascript"></script>
	<!-- <script src="resources/etc/jquery/jquery-validate.bootstrap-tooltip.js"></script> -->
	<script src="resources/etc/jquery/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
	<script>
	$("#role").val($("#role option:first").val());
	$("input[name=userId]").focus();
		var i1 = "User ID";
		var i2 = "User Name";
		var i3 = "Role";
		var i4 = "Password";
		$(document).ready(function () {
			
		    $("#formUser").validate({
		    	rules: {
		    		userId: {required: true},
		    		userName: {required: true},
		    		userRole: {required: true},
		    		userPassword: {required: true}
	            },
	            messages: {
	            	userId: {required: i1+" harus diisi !!!"},
		    		userName: {required: i2+" harus diisi !!!"},
		    		userRole: {required: i3+" harus diisi !!!"},
		    		userPassword: {required: i4+" harus diisi !!!"}
	            },
	            /* tooltip_options: {
	            	coba: {placement:'right', html:true}
            	}, */
	            submitHandler: function(form) { form.submit(); }
		    });
		});
		function backUser(src) {
			parent.content.location = src;
		}
	
	$('a.eng').click(function(e){
		if (confirm("Yakin ingin merubah bahasa ?")) {
	    } else {
	    	e.preventDefault();
	    }
	});
	
	</script>
</body>
</html>