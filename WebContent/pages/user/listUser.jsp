<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/main.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CCOS-BCA</title>
</head>
<body>
	<html:form action="/user">
	<table border="1" class="input">
			<tr>
				<th colspan="2" class="top">Enter Search Criteria</th>
			</tr>
			<tr>
				<td class="inlabel">User ID</td>
				<td><html:text property="searchUserId"/></td>
			</tr>
			<tr>
				<td class="inlabel">User Name</td>
				<td><html:text property="searchUserName"/></td>
			</tr>
			<tr>
				<td colspan="2" class="infoot">
				<input type="button" value="Refresh" onclick="doRefresh()"/> &nbsp; <input type="button" value="Search" onclick="searchPage()"/>
				</td>
				
			</tr>
		</table>
		
		<table border="1" class="view">
			<tr>
				<th colspan="8" class="top">User List</th>
			</tr>
			<tr>
				<th >User ID</th>
				<th >User Name</th>
				<th >Role</th>
				<th >Date Created</th>
				<th >Created By</th>
				<th >Last Update</th>
				<th >Update By</th>
				<th>Opsi</th>
			</tr>
			<logic:empty name="userForm" property="listUser">
			<tr>
				<td colspan="8" class="center blue">Data is not found</td>
			</tr>
			</logic:empty>
			<logic:notEmpty name="userForm" property="listUser">
			<logic:iterate id="idlist" name="userForm" property="listUser">
				<tr>
					<td><a href="javascript:openDetail('<bean:write name="idlist" property="userId"/>')"><bean:write name="idlist" property="userId" /></a></td>
					<td><bean:write name="idlist" property="userName" /></td>
					<td><bean:write name="idlist" property="userRole" /></td>
					<td><bean:write name="idlist" property="dateCreated" /></td>
					<td><bean:write name="idlist" property="createdBy" /></td>
					<td><bean:write name="idlist" property="lastUpdate" /></td>
					<td><bean:write name="idlist" property="updateBy" /></td>
					<td>
					<button class="btn bblue" onclick="showEdit('<bean:write name="idlist" property="userId"/>')">Edit</button>
					<button class="btn bred" onclick="showDelete('<bean:write name="idlist" property="userId"/>')">Delete</button>
					</td>
				</tr>
			</logic:iterate>
			<tr>
				<td colspan="8" class="infoot">
				<input type="button" class="nav left" value="<<"/> &nbsp; 
				<input type="button" class="nav left" value="<"/> &nbsp; 
				<input type="button" class="nav left" value=">"/> &nbsp; 
				<input type="button" class="nav left" value=">>"/> &nbsp;
				<span class="search left">&nbsp;&nbsp;&nbsp;Page&nbsp;</span>
				<input type="text" class="search left"/>
				<input type="button" class="nav left" value="Go"/> &nbsp;
				<span class="search right">&nbsp;&nbsp;&nbsp;Total 1000 Record(s), Page 1 of 2&nbsp;</span>
			</tr>
			</logic:notEmpty>
			<tr>
				<td colspan="8" class="infoot">
				<a href="javascript:addPage('user.do?task=showAdd');"><input type="button" value="Add" /></a>
				</td>
			</tr>
		</table>
		<html:hidden property="task"/>
		<html:hidden property="userId"/>
		</html:form>
		<script type="text/javascript">
		function doRefresh(){
			document.forms[0].action="user.do";
			document.forms[0].task.value="showUser";
			document.forms[0].submit();
		}
		function searchPage(){
			document.forms[0].action="user.do";
			document.forms[0].task.value="doSearch";
			document.forms[0].submit();
		}
		function showEdit(code) {
			document.forms[0].task.value = "showEdit";
			document.forms[0].userId.value = code;
			document.forms[0].submit();
		}
		function showDelete(code){
			document.forms[0].task.value = "doDelete";
			document.forms[0].userId.value = code;
			document.forms[0].submit();
		}
		function addPage(src) {
			parent.content.location = src;
		}
		
		</script>
</body>
</html>