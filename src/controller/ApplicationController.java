package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.ApplicationForm;
import service.ApplicationService;

public class ApplicationController extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		ApplicationForm frm = (ApplicationForm) form; 
		String pageForward = "showApplication";
		String task = frm.getTask();
		System.out.println("-------------task = application." + task);
		ApplicationService applicationService = new ApplicationService();
		if("showApplication".equals(task)) {
			applicationService.getListApplication(frm);
			pageForward = "showApplication";
		}else if("showAdd".equals(task)) {
			pageForward = "addApplication";
		}else if("doAdd".equals(task)) {
			frm.setStatus("Pending Data Entry");
			frm.setHoldBy((String)session.getAttribute("holdBy"));
			frm.setRefNo(applicationService.getNumberReff(frm));
			applicationService.insertApplication(frm);
			applicationService.getListApplication(frm);
			pageForward = "showApplication";
		}else if("doSearch".equals(task)) {
			pageForward = "showApplication";
			applicationService.getSearchApplication(frm);
		}
		return mapping.findForward(pageForward);
	}
}
