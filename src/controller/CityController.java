package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.CityForm;
import service.CityService;

public class CityController extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		CityForm frm = (CityForm) form; 
		String task = frm.getTask();
		String pageForward = "showCity";
		CityService cityService = new CityService();
		if ("showCity".equals(task)) {
			cityService.getListCity(frm);
			pageForward = "showCity";
		}else if("showAdd".equals(task)) {
			pageForward = "addCity";
		}else if("doAdd".equals(task)) {
			cityService.insertCity(frm);
			cityService.getListCity(frm);
			pageForward = "showCity";
		}else if("doSearch".equals(task)) {
			cityService.getSearchCity(frm);
			pageForward = "showCity";
		}else if("doDelete".equals(task)) {
			cityService.deleteCity(frm);
			cityService.getListCity(frm);
			pageForward = "showCity";
		}else if("showEdit".equals(task)) {
			cityService.getCityById(frm);
			pageForward = "editCity";
		}else if("doUpdate".equals(task)) {
			cityService.updateCity(frm);
			cityService.getListCity(frm);
			pageForward = "showCity";
		}else if("windowCity".equals(task)) {
			cityService.getListCity(frm);
			pageForward = "getCity";
		}else if("doGetSearch".equals(task)) {
			cityService.getSearchCity(frm);
			pageForward = "getCity";
		}
		return mapping.findForward(pageForward);
	}
}
