package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.CustomerForm;
import service.CustomerService;

public class CustomerController extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		CustomerForm frm = (CustomerForm) form; 
		String pageForward = "showCustomer";
		String task = frm.getTask();
		System.out.println("-------------task = cutomer." + task);
		CustomerService customerService = new CustomerService();
		if("showHome".equals(task)) {
			pageForward = "home";
		}else if ("showCustomer".equals(task)) {
			pageForward = "showCustomer";
			customerService.getListCustomer(frm);
		}else if("showAdd".equals(task)) {
			customerService.getListProvince(frm);
			pageForward = "addCustomer";
		}else if("doAdd".equals(task)) {
//			frm.setCustId("2");
			customerService.getCustId(frm);
			customerService.insertCustomer(frm);
			customerService.insertCustomerSpouse(frm);
			customerService.insertCustomerAdrs(frm);
			customerService.getCustomerById(frm);
//			customerService.getCustomerSpouseById(frm);
			customerService.getCustomerAdrsById(frm);
			customerService.getListApplication(frm);
			pageForward = "showSummary";
		}else if("doSearch".equals(task)) {
			pageForward = "showCustomer";
			customerService.getSearchCustomer(frm);
		}
		return mapping.findForward(pageForward);
	}

}
