package controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.google.gson.Gson;

import form.LoginForm;
import service.LoginService;

public class LoginController extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		LoginForm frm = (LoginForm) form; // Casting ActionForm ke LoginForm untuk menggunakan getTask
		String pageForward = "login";
		String task = frm.getTask();
		System.out.println("-------------task = login." + task);
//		System.out.println("Session id =  " + session.getId());
		LoginService loginService = new LoginService();
		HashMap<String, String> mapOutput = new HashMap<String, String>();
		mapOutput.put("userId", frm.getUserId());
		mapOutput.put("Password", frm.getUserPassword());

		Gson gson = new Gson();
		String json = gson.toJson(mapOutput);

		
		if ("showHeader".equals(task)) {
			pageForward = "header";
		} else if ("showMenu".equals(task)) {
			pageForward = "menu";
		} else if ("showContent".equals(task)) {
			pageForward = "content";
		} else if ("showFooter".equals(task)) {
			pageForward = "footer";
		} else if ("doLogout".equals(task)) {
			session.invalidate();
			pageForward = "login";
		} else if ("doLogin".equals(task)) {
			if (null != session.getAttribute("userId")) {
				pageForward = "home";
			} else {
				if (loginService.dologin(frm)) {
					loginService.getUserById(frm);
					pageForward = "home";
					session.setAttribute("userId", frm.getUserId());
					session.setAttribute("userName", frm.getUserName());
					session.setAttribute("userRole", frm.getUserRole());
					session.setAttribute("holdBy", frm.getUserId()+" ("+frm.getUserName()+")");
					System.out.println("JSON Parameter : \n" + json);
					// System.out.println("------------<>SESSION<>"+session.getAttribute("userName"));
				} else {
					pageForward = "login";
				}
			}
		}
		return mapping.findForward(pageForward);
	}
}
