package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.ProvinceForm;
import service.ProvinceService;

public class ProvinceController extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		ProvinceForm frm = (ProvinceForm) form; 
		String task = frm.getTask();
		System.out.println("-------------task = province." + task);
		String pageForward = "showProvince";
		ProvinceService provinceService = new ProvinceService();
		if ("showProvince".equals(task)) {
			provinceService.getListProvince(frm);
			pageForward = "showProvince";
		}else if("showAdd".equals(task)) {
			pageForward = "addProvince";
		}else if("doAdd".equals(task)) {
			provinceService.insertProvince(frm);
			provinceService.getListProvince(frm);
			pageForward = "showProvince";
		}else if("doSearch".equals(task)) {
			provinceService.getSearchProvince(frm);
			pageForward = "showProvince";
		}else if("doDelete".equals(task)) {
			provinceService.deleteProvince(frm);
			provinceService.getListProvince(frm);
			pageForward = "showProvince";
		}else if("showEdit".equals(task)) {
			provinceService.getProvinceById(frm);
			pageForward = "editProvince";
		}else if("doUpdate".equals(task)) {
			provinceService.updateProvince(frm);
			provinceService.getListProvince(frm);
			pageForward = "showProvince";
		}else if("windowProvince".equals(task)) {
			provinceService.getListProvince(frm);
			pageForward = "getProvince";
		}else if("doGetSearch".equals(task)) {
			provinceService.getSearchProvince(frm);
			pageForward = "getProvince";
		}
		return mapping.findForward(pageForward);
	}
}
