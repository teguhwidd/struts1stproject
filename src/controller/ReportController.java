package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.ReportForm;
import service.ReportService;

public class ReportController extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		ReportForm frm = (ReportForm) form; 
		String pageForward = "showReport";
		String task = frm.getTask();
		System.out.println("-------------task = report." + task);
		if ("showReport".equals(task)) {
			frm.setRadioLaporanApp("yes");
			pageForward = "pageReport";
		}else if("generateReport".equals(task)) {
			String mYear = frm.getMonth()+frm.getYear();
			ReportService.getReport(mYear);
			pageForward = "showReport";
		}else if("showReport2".equals(task)) {
			pageForward = "showReport2";
		}
		return mapping.findForward(pageForward);
		
	}
}
