package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.UserForm;
import service.UserService;

public class UserController extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		UserForm frm = (UserForm) form; 
		frm.setCreatedBy("coba");
		frm.setUpdateBy("coba");
		String task = frm.getTask();
		String pageForward = "showUser";
		UserService userService = new UserService();
		if ("showUser".equals(task)) {
			pageForward = "showUser";
			userService.getListUser(frm);
		}else if("showAdd".equals(task)) {
			pageForward = "addUser";
		}else if("doAdd".equals(task)) {
			userService.insertUser(frm);
			userService.getListUser(frm);
			pageForward = "showUser";
		}else if("doSearch".equals(task)) {
			pageForward = "showUser";
			userService.getSearchUser(frm);
		}else if("doDelete".equals(task)) {
			userService.deleteUser(frm);
			userService.getListUser(frm);
			pageForward = "showUser";
		}else if("showEdit".equals(task)) {
			userService.getUserById(frm);
			pageForward = "editUser";
		}else if("doUpdate".equals(task)) {
			userService.updateUser(frm);
			userService.getListUser(frm);
			pageForward = "showProvince";
		}
		return mapping.findForward(pageForward);
	}
}
