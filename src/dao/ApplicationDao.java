package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import form.ApplicationForm;
import setting.DBConnection;
import util.DateFormater;

public class ApplicationDao {
Connection conn;
	
	public void insertApplication(ApplicationForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "INSERT INTO trx_application "
				+"(ref_no, refferal_branch,"
				+ "date_received, facility, application_purpose,"
				+ "business_source, media_channel, fee_branch,"
				+ "provision_branch, kckk_branch, staff_name,"
				+ "staff_nip, staff_branch, staff_account_num,"
				+ "status, hold_by, cust_id) "
				+"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getRefNo());
			ps.setString(2, frm.getRefferalBranch());
			ps.setDate(3, DateFormater.strToSqlDate(frm.getDateReceived()));
			ps.setString(4, frm.getFacility());
			ps.setString(5, frm.getApplicationPurpose());
			ps.setString(6, frm.getBusinessSource());
			ps.setString(7, frm.getMediaChannel());
			ps.setString(8, frm.getFeeBranch());
			ps.setString(9, frm.getProvisionBranch());
			ps.setString(10, frm.getKckkBranch());
			ps.setString(11, frm.getStaffName());
			ps.setString(12, frm.getStaffNip());
			ps.setString(13, frm.getStaffBranch());
			ps.setString(14, frm.getStaffAccountNum());
			ps.setString(15, frm.getStatus());
			ps.setString(16, frm.getHoldBy());
			ps.setString(17, frm.getCustId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public ApplicationForm getApplicationById(ApplicationForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_application WHERE ref_no = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getRefNo());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setRefNo(rs.getString("ref_no"));
				frm.setRefferalBranch(rs.getString("refferal_branch"));
				frm.setDateReceived(DateFormater.dateToStr(rs.getDate("date_received"),"dd-MMM-yyyy"));
				frm.setFacility(rs.getString("facility"));
				frm.setApplicationPurpose(rs.getString("application_purpose"));
				frm.setBusinessSource(rs.getString("business_source"));
				frm.setMediaChannel(rs.getString("media_channel"));
				frm.setFeeBranch(rs.getString("fee_branch"));
				frm.setProvisionBranch(rs.getString("provision_branch"));
				frm.setKckkBranch(rs.getString("kckk_branch"));
				frm.setStaffName(rs.getString("staff_name"));
				frm.setStaffNip(rs.getString("staff_nip"));
				frm.setStaffBranch(rs.getString("staff_branch"));
				frm.setStaffAccountNum(rs.getString("staff_account_num"));
				frm.setStatus(rs.getString("status"));
				frm.setHoldBy(rs.getString("hold_by"));
				frm.setCustId(rs.getString("cust_id"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
	public ArrayList<HashMap<String, String>> getListAllApplication() throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_application";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("refNo", rs.getString("ref_no"));
				map.put("refferalBranch", rs.getString("refferal_branch"));
				map.put("dateReceived", DateFormater.dateToStr(rs.getDate("date_received"),"dd-MMM-yyyy"));
				map.put("facility", rs.getString("facility"));
				map.put("applicationPurpose", rs.getString("application_purpose"));
				map.put("businessSource", rs.getString("business_source"));
				map.put("mediaChannel", rs.getString("media_channel"));
				map.put("feeBranch", rs.getString("fee_branch"));
				map.put("provisionBranch", rs.getString("provision_branch"));
				map.put("kckkBranch", rs.getString("kckk_branch"));
				map.put("staffName", rs.getString("staff_name"));
				map.put("staffNip", rs.getString("staff_nip"));
				map.put("staffBranch", rs.getString("staff_branch"));
				map.put("staffAccountNum", rs.getString("staff_account_num"));
				map.put("status", rs.getString("status"));
				map.put("holdBy", rs.getString("hold_by"));
				map.put("custId", rs.getString("cust_id"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	public ArrayList<HashMap<String, String>> getSearchApplication(ApplicationForm frm) throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_application WHERE "
				+ "ref_no =? OR staff_name =? OR date_received =?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getRefNo());
			ps.setString(2, frm.getStaffName());
			ps.setDate(3, DateFormater.strToSqlDate(frm.getDateReceived()));
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("refNo", rs.getString("ref_no"));
				map.put("refferalBranch", rs.getString("refferal_branch"));
				map.put("dateReceived", DateFormater.dateToStr(rs.getDate("date_received"),"dd-MMM-yyyy"));
				map.put("facility", rs.getString("facility"));
				map.put("applicationPurpose", rs.getString("application_purpose"));
				map.put("businessSource", rs.getString("business_source"));
				map.put("mediaChannel", rs.getString("media_channel"));
				map.put("feeBranch", rs.getString("fee_branch"));
				map.put("provisionBranch", rs.getString("provision_branch"));
				map.put("kckkBranch", rs.getString("kckk_branch"));
				map.put("staffName", rs.getString("staff_name"));
				map.put("staffNip", rs.getString("staff_nip"));
				map.put("staffBranch", rs.getString("staff_branch"));
				map.put("staffAccountNum", rs.getString("staff_account_num"));
				map.put("status", rs.getString("status"));
				map.put("holdBy", rs.getString("hold_by"));
				map.put("custId", rs.getString("cust_id"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	
	public Integer getRowNumApplication() throws Exception{
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT count(*) as total FROM trx_application";
		int num = 0;
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if(rs.next()) {
				num = (rs.getInt("total"));
				if(0 == num) {
					num = 1;
				}else {
					num = num + 1;
				}	
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return  num;
	}
	
	public String getRowNumApplication2() throws Exception{
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT count(*) as total FROM trx_application";
		int num = 0;
		String temp = null;
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if(rs.next()) {
				temp = (rs.getString("total"));
				if("0".equals(temp)) {
					temp = "1";
				}else {
					num = Integer.parseInt(temp) + 1;
					temp = Integer.toString(num);
				}	
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return  temp;
	}
}
