package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import form.CityForm;
import setting.DBConnection;

public class CityDao {
Connection conn;
	
	public void insertCity(CityForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "INSERT INTO mst_city "
				+"(city_id,city_name,province_id)"
				+"VALUES(?,?,?)";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCityId());
			ps.setString(2, frm.getCityName());
			ps.setString(3, frm.getProvinceId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void updateCity(CityForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "UPDATE mst_city SET "
				+"city_name = ?, proovince_id = ? "
				+"WHERE city_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCityName());
			ps.setString(2, frm.getProvinceId());
			ps.setString(3, frm.getCityId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void deleteCity(CityForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "DELETE FROM mst_city WHERE "
				+"city_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCityId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public CityForm getCityById(CityForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM mst_city WHERE city_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCityId());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setCityId(rs.getString("city_id"));
				frm.setCityName(rs.getString("city_name"));
				frm.setProvinceId(rs.getString("province_id"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
	public ArrayList<HashMap<String, String>> getListAllCity() throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
//		String sql = "SELECT * FROM mst_city";
		String sql = "SELECT city.city_id, city.city_name, prov.province_id, prov.province_name FROM mst_city city"
				+ " LEFT OUTER JOIN mst_province prov ON city.province_id = prov.province_id ORDER BY city_name";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("cityId", rs.getString("city_id"));
				map.put("cityName", rs.getString("city_name"));
				map.put("provinceId", rs.getString("province_id"));
				map.put("provinceName", rs.getString("province_name"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	public ArrayList<HashMap<String, String>> getSearchCity(CityForm frm) throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM mst_city WHERE city_name = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getSearchCityName());
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("cityId", rs.getString("city_id"));
				map.put("cityName", rs.getString("city_name"));
				map.put("provinceId", rs.getString("province_id"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
}
