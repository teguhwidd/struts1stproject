package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import form.CustomerForm;
import setting.DBConnection;
import util.DateFormater;

public class CustomerDao {
	Connection conn;
	
	public void insertCustomer(CustomerForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "INSERT INTO trx_customer "
				+"(cust_id,cust_name, cust_fullname,"
				+ "cust_id_type, cust_id_number, cust_id_exp,"
				+ "cust_gender, cust_plob, cust_cob,"
				+ "cust_prob, cust_dob, cust_marital_status,"
				+ "cust_num_of_dependents, cust_lvl_of_education, cust_mother_maiden)"
				+"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCustId());
			ps.setString(2, frm.getCustName());
			ps.setString(3, frm.getCustFullName());
			ps.setString(4, frm.getCustIdType());
			ps.setString(5, frm.getCustIdNumber());
			ps.setDate(6, DateFormater.strToSqlDate(frm.getCustIdExp()));
			ps.setString(7, frm.getCustGender());
			ps.setString(8, frm.getCustPlaceOfBirth());
			ps.setString(9, frm.getCustCityOfBirth());
			ps.setString(10, frm.getCustProvinceOfBirth());
			ps.setDate(11, DateFormater.strToSqlDate(frm.getCustDateOfBirth()));
			ps.setString(12, frm.getCustMaritalStatus());
			ps.setString(13, frm.getCustNumOfDependents());
			ps.setString(14, frm.getCustLvlOfEducation());
			ps.setString(15, frm.getCustMotherMaiden());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void insertCustomerSpouse(CustomerForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "INSERT INTO trx_cust_spouse "
				+"(cust_id, cust_spouse_name,"
				+ "cust_spouse_id, cust_spouse_dob, cust_spouse_pa)"
				+"VALUES(?,?,?,?,?)";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCustId());
			ps.setString(2, frm.getCustSpouseName());
			ps.setString(3, frm.getCustSpouseId());
			ps.setDate(4, DateFormater.strToSqlDate(frm.getCustSpouseDob()));
			ps.setString(5, frm.getCustSpousePA());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void insertCustomerAddress(CustomerForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "INSERT INTO trx_customer_address "
				+"(cust_id, mailing_address,"
				+ "cust_address1, cust_address2, cust_address3,"
				+ "cust_address4, cust_adrs_city, cust_adrs_province,"
				+ "cust_adrs_zip_code, cust_adrs_kelurahan,cust_adrs_kecamatan,"
				+ "cust_adrs_home_phone, cust_adrs_mobile_phone, "
				+ "cust_adrs_email, cust_adrs_res_status, cust_adrs_los)"
				+"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCustId());
			ps.setString(2, frm.getMailingAddress());
			ps.setString(3, frm.getCustAddress1());
			ps.setString(4, frm.getCustAddress2());
			ps.setString(5, frm.getCustAddress3());
			ps.setString(6, frm.getCustAddress4());
			ps.setString(7, frm.getCustAdrsCity());
			ps.setString(8, frm.getCustAdrsProvince());
			ps.setString(9, frm.getCustAdrsZipCode());
			ps.setString(10, frm.getCustAdrsKelurahan());
			ps.setString(11, frm.getCustAdrsKecamatan());
			ps.setString(12, frm.getCustAdrsHomePhone());
			ps.setString(13, frm.getCustAdrsMobilePhone());
			ps.setString(14, frm.getCustAdrsEmail());
			ps.setString(15, frm.getCustAdrsResStatus());
			ps.setString(16, frm.getCustAdrsLos());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public CustomerForm getCustomerAdrsById(CustomerForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_customer_address WHERE cust_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCustId());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setCustId(rs.getString("cust_id"));
				frm.setMailingAddress(rs.getString("mailing_address"));
				frm.setCustAddress1(rs.getString("cust_address1"));
				frm.setCustAddress2(rs.getString("cust_address2"));
				frm.setCustAddress3(rs.getString("cust_address3"));
				frm.setCustAddress4(rs.getString("cust_address4"));
				frm.setCustAdrsCity(rs.getString("cust_adrs_city"));
				frm.setCustAdrsProvince(rs.getString("cust_adrs_province"));
				frm.setCustAdrsZipCode(rs.getString("cust_adrs_zip_code"));
				frm.setCustAdrsKelurahan(rs.getString("cust_adrs_kelurahan"));
				frm.setCustAdrsKecamatan(rs.getString("cust_adrs_kecamatan"));
				frm.setCustAdrsHomePhone(rs.getString("cust_adrs_home_phone"));
				frm.setCustAdrsMobilePhone(rs.getString("cust_adrs_mobile_phone"));
				frm.setCustAdrsEmail(rs.getString("cust_adrs_email"));
				frm.setCustAdrsResStatus(rs.getString("cust_adrs_res_status"));
				frm.setCustAdrsLos(rs.getString("cust_adrs_los"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
	public CustomerForm getCustomerSpouseById(CustomerForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_customer_spouse WHERE cust_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCustId());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setCustId(rs.getString("cust_id"));
				frm.setCustSpouseName(rs.getString("cust_spouse_name"));
				frm.setCustSpouseId(rs.getString("cust_spouse_id"));
				frm.setCustSpouseDob(rs.getString("cust_spouse_dob"));
				frm.setCustSpousePA(rs.getString("cust_spouse_pa"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
	public CustomerForm getCustomerById(CustomerForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_customer WHERE cust_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getCustId());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setCustId(rs.getString("cust_id"));
				frm.setCustName(rs.getString("cust_name"));
				frm.setCustFullName(rs.getString("cust_fullname"));
				frm.setCustIdType(rs.getString("cust_id_type"));
				frm.setCustIdNumber(rs.getString("cust_id_number"));
				frm.setCustIdExp(DateFormater.dateToStr(rs.getDate("cust_id_exp"),"dd-MMM-yyyy"));
				frm.setCustGender(rs.getString("cust_gender"));
				frm.setCustPlaceOfBirth(rs.getString("cust_plob"));
				frm.setCustCityOfBirth(rs.getString("cust_cob"));
				frm.setCustProvinceOfBirth(rs.getString("cust_prob"));
				frm.setCustDateOfBirth(DateFormater.dateToStr(rs.getDate("cust_dob"),"dd-MMM-yyyy"));
				frm.setCustMaritalStatus(rs.getString("cust_marital_status"));
				frm.setCustNumOfDependents(rs.getString("cust_num_of_dependents"));
				frm.setCustLvlOfEducation(rs.getString("cust_lvl_of_education"));
				frm.setCustMotherMaiden(rs.getString("cust_mother_maiden"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
	public ArrayList<HashMap<String, String>> getListAllCustumer() throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_customer";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("custId", rs.getString("cust_id"));
				map.put("custName", rs.getString("cust_name"));
				map.put("custFullName", rs.getString("cust_fullname"));
				map.put("custIdType", rs.getString("cust_id_type"));
				map.put("custIdNumber", rs.getString("cust_id_number"));
				map.put("custIdExp", DateFormater.dateToStr(rs.getDate("cust_id_exp"),"dd-MMM-yyyy"));
				map.put("custGender", rs.getString("cust_gender"));
				map.put("custPlaceOfBirth", rs.getString("cust_plob"));
				map.put("custCountryOfBirth", rs.getString("cust_cob"));
				map.put("custProvinceOfBirth", rs.getString("cust_prob"));
				map.put("custDateOfBirth", DateFormater.dateToStr(rs.getDate("cust_dob"),"dd-MMM-yyyy"));
				map.put("custMaritalStatus", rs.getString("cust_marital_status"));
				map.put("custNumOfDependents", rs.getString("cust_num_of_dependents"));
				map.put("custLvlOfEducation", rs.getString("cust_lvl_of_education"));
				map.put("custMotherMaiden", rs.getString("cust_mother_maiden"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	public ArrayList<HashMap<String, String>> getSearchCustomer(CustomerForm frm) throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM trx_customer WHERE "
				+ "cust_name =? OR cust_id_number =? OR cust_dob =?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getSearchCustName());
			ps.setString(2, frm.getSearchCustIdNumber());
			ps.setDate(3, DateFormater.strToSqlDate(frm.getSearchCustDateOfBirth()));
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("custId", rs.getString("cust_id"));
				map.put("custName", rs.getString("cust_name"));
				map.put("custFullName", rs.getString("cust_fullname"));
				map.put("custIdType", rs.getString("cust_id_type"));
				map.put("custIdNumber", rs.getString("cust_id_number"));
				map.put("custIdExp", DateFormater.dateToStr(rs.getDate("cust_id_exp"),"dd-MMM-yyyy"));
				map.put("custGender", rs.getString("cust_gender"));
				map.put("custPlaceOfBirth", rs.getString("cust_plob"));
				map.put("custCountryOfBirth", rs.getString("cust_cob"));
				map.put("custProvinceOfBirth", rs.getString("cust_prob"));
				map.put("custDateOfBirth", DateFormater.dateToStr(rs.getDate("cust_dob"),"dd-MMM-yyyy"));
				map.put("custMaritalStatus", rs.getString("cust_marital_status"));
				map.put("custNumOfDependents", rs.getString("cust_num_of_dependents"));
				map.put("custLvlOfEducation", rs.getString("cust_lvl_of_education"));
				map.put("custMotherMaiden", rs.getString("cust_mother_maiden"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	public Integer getRowNumCustomer() throws Exception{
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT max(cust_id) as jum FROM trx_customer";
		int num = 0;
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if(rs.next()) {
				num = (rs.getInt("jum"));
				if(0 == num) {
					num = 1;
				}else {
					num = num + 1;
				}	
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return  num;
	}
}
