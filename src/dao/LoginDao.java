package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import form.LoginForm;
import setting.DBConnection;

public class LoginDao {
	Connection conn;
	public boolean isUserExist(String user_id, String user_password) throws Exception{
		boolean retval = false;
		PreparedStatement ps;
		ResultSet rs;
		String sql = "SELECT 1 FROM sec_users "
					+ "WHERE user_id=? AND user_password=?";
		try {
			conn = DBConnection.getConnection();
//			conn = DBConnection.getConnection(DBConnection.DS_NAME);
			ps = conn.prepareStatement(sql);
			ps.setString(1, user_id);
			ps.setString(2, user_password);
			rs = ps.executeQuery();
			if(rs.next()) {
				retval = true;
			}
			rs.close();
			ps.close();
		}finally {
			conn.close();
		}
		return retval;
	}
	public LoginForm getUserById(LoginForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM sec_users WHERE user_id = ?";
		
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getUserId());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setUserId(rs.getString("user_id"));
				frm.setUserName(rs.getString("user_name"));
				frm.setUserRole(rs.getString("user_role"));
				frm.setUserPassword(rs.getString("user_password"));
				frm.setDateCreated(rs.getString("date_created"));
				frm.setCreatedBy(rs.getString("created_by"));
				frm.setLastUpdate(rs.getString("last_update"));
				frm.setUpdateBy(rs.getString("update_by"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
}
