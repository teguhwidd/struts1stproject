package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import form.ProvinceForm;
import setting.DBConnection;

public class ProvinceDao {
Connection conn;
	
	public void insertPrvince(ProvinceForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "INSERT INTO mst_province "
				+"(province_id,province_name)"
				+"VALUES(?,?)";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getProvinceId());
			ps.setString(2, frm.getProvinceName());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void updatePrvince(ProvinceForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "UPDATE mst_province SET "
				+"province_name = ? "
				+"WHERE province_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getProvinceName());
			ps.setString(2, frm.getProvinceId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void deletePrvince(ProvinceForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "DELETE FROM mst_province WHERE "
				+"province_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getProvinceId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public ProvinceForm getProvinceById(ProvinceForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM mst_province WHERE province_id = ?";
//		String sql = "SELECT prov.province_id, prov.province_name, city.city_id, city.city_name FROM"
//				+ " mst_province prov LEFT OUTER JOIN mst_city city ON prov.province_id = "
//				+ "city.province_id WHERE province_id = ? ORDER BY prov.province_name";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getProvinceId());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setProvinceId(rs.getString("province_id"));
				frm.setProvinceName(rs.getString("province_name"));
//				frm.setCityId(rs.getString("city_id"));
//				frm.setCityName(rs.getString("city_name"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
	public ArrayList<HashMap<String, String>> getListAllProvince() throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM mst_province";
//		String sql = "SELECT prov.province_id, prov.province_name, city.city_id, city.city_name FROM mst_province prov"
//				+ " LEFT OUTER JOIN mst_city city ON prov.province_id = city.province_id  ORDER BY city_name";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("provinceId", rs.getString("province_id"));
				map.put("provinceName", rs.getString("province_name"));
//				map.put("cityId", rs.getString("city_id"));
//				map.put("cityName", rs.getString("city_name"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	public ArrayList<HashMap<String, String>> getSearchProvince(ProvinceForm frm) throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM mst_province WHERE province_name = ?";
//		String sql = "SELECT prov.province_id, prov.province_name, city.city_id, city.city_name "
//				+ "FROM mst_province prov LEFT OUTER JOIN mst_city city ON prov.province_id = "
//				+ "city.province_id WHERE province_name = ?  ORDER BY prov.province_name";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getSearchProvinceName());
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("provinceId", rs.getString("province_id"));
				map.put("provinceName", rs.getString("province_name"));
//				map.put("cityId", rs.getString("city_id"));
//				map.put("cityName", rs.getString("city_name"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
}
