package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import form.UserForm;
import setting.DBConnection;
import util.DateFormater;

public class UserDao {
	Connection conn;
	
	public void insertUser(UserForm frm) throws Exception {
		PreparedStatement ps;
		String dateCreated = DateFormater.getCurDate("dd/MM/yyyy");
		String dateUpdate = DateFormater.getCurDate("dd/MM/yyyy");
		System.out.println("================"+dateCreated);
		String sql = "INSERT INTO sec_users "
				+"(user_id,user_name,user_role,user_password,date_created,created_by,last_update,update_by)"
				+"VALUES(?,?,?,?,'"+dateCreated+"',?,'"+dateUpdate+"',?)";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getUserId());
			ps.setString(2, frm.getUserName());
			ps.setString(3, frm.getUserRole());
			ps.setString(4, frm.getUserPassword());
			ps.setString(5, frm.getCreatedBy());
			ps.setString(6, frm.getUpdateBy());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void updateUser(UserForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "UPDATE sec_users SET "
				+"user_name = ? , user_role = ? , user_password = ? ,"
				+ " last_update = ?, update_by = ?"
				+"WHERE user_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getUserName());
			ps.setString(2, frm.getUserRole());
			ps.setString(3, frm.getUserPassword());
			ps.setString(4, frm.getLastUpdate());
			ps.setString(5, frm.getUpdateBy());
			ps.setString(5, frm.getUserId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public void deleteUser(UserForm frm) throws Exception {
		PreparedStatement ps;
		String sql = "DELETE FROM sec_users WHERE "
				+"user_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getUserId());
			ps.executeQuery();
			ps.close();
		}
		finally {
			conn.close();
		}
	}
	public UserForm getUserById(UserForm frm) throws Exception {
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM sec_users WHERE user_id = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getUserId());
			rs = ps.executeQuery();
			if(rs.next()) {
				frm.setUserId(rs.getString("user_id"));
				frm.setUserName(rs.getString("user_name"));
				frm.setUserRole(rs.getString("user_role"));
				frm.setUserPassword(rs.getString("user_password"));
				frm.setDateCreated(rs.getString("date_created"));
				frm.setCreatedBy(rs.getString("created_by"));
				frm.setLastUpdate(rs.getString("last_update"));
				frm.setUpdateBy(rs.getString("update_by"));
			}
			rs.close();
			ps.close();
		}
		finally {
			conn.close();
		}
		return frm;
	}
	public ArrayList<HashMap<String, String>> getListAllUser() throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM sec_users";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("userId", rs.getString("user_id"));
				map.put("userName", rs.getString("user_name"));
				map.put("userRole", rs.getString("user_role"));
				map.put("userPassword", rs.getString("user_password"));
				map.put("dateCreated", rs.getString("date_created"));
				map.put("createdBy", rs.getString("created_by"));
				map.put("lastUpdate", rs.getString("last_update"));
				map.put("updateBy", rs.getString("update_by"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
	public ArrayList<HashMap<String, String>> getSearchUser(UserForm frm) throws Exception{
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map;
		PreparedStatement ps;
		ResultSet rs = null;
		String sql = "SELECT * FROM sec_users WHERE user_id = ? OR user_name = ?";
		try {
			conn = DBConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, frm.getSearchUserId());
			ps.setString(2, frm.getSearchUserName());
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<>();
				map.put("userId", rs.getString("user_id"));
				map.put("userName", rs.getString("user_name"));
				map.put("userRole", rs.getString("user_role"));
				map.put("userPassword", rs.getString("user_password"));
				map.put("dateCreated", rs.getString("date_created"));
				map.put("createdBy", rs.getString("created_by"));
				map.put("lastUpdate", rs.getString("last_update"));
				map.put("updateBy", rs.getString("update_by"));
				list.add(map);
			}
			rs.close();
			ps.close();
		} finally {
			conn.close();
		}
		return list;
	}
}
