package form;

import java.util.ArrayList;
import java.util.HashMap;

public class ApplicationForm extends BaseForm {
	private String custId;
	private String refNo;
	private String refferalBranch;
	private String dateReceived;
	private String facility;
	private String applicationPurpose;
	private String businessSource;
	private String mediaChannel;
	private String feeBranch;
	private String provisionBranch;
	private String kckkBranch;
	private String staffName;
	private String staffNip;
	private String staffBranch;
	private String staffAccountNum;
	private String status;
	private String holdBy;
	private ArrayList<HashMap<String, String>> listApplication;
	
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHoldBy() {
		return holdBy;
	}
	public void setHoldBy(String holdBy) {
		this.holdBy = holdBy;
	}

	public String getRefferalBranch() {
		return refferalBranch;
	}
	public void setRefferalBranch(String refferalBranch) {
		this.refferalBranch = refferalBranch;
	}
	public String getDateReceived() {
		return dateReceived;
	}
	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getApplicationPurpose() {
		return applicationPurpose;
	}
	public void setApplicationPurpose(String applicationPurpose) {
		this.applicationPurpose = applicationPurpose;
	}
	public String getMediaChannel() {
		return mediaChannel;
	}
	public void setMediaChannel(String mediaChannel) {
		this.mediaChannel = mediaChannel;
	}
	public String getBusinessSource() {
		return businessSource;
	}
	public void setBusinessSource(String businessSource) {
		this.businessSource = businessSource;
	}
	public String getFeeBranch() {
		return feeBranch;
	}
	public void setFeeBranch(String feeBranch) {
		this.feeBranch = feeBranch;
	}
	public String getProvisionBranch() {
		return provisionBranch;
	}
	public void setProvisionBranch(String provisionBranch) {
		this.provisionBranch = provisionBranch;
	}
	public String getKckkBranch() {
		return kckkBranch;
	}
	public void setKckkBranch(String kckkBranch) {
		this.kckkBranch = kckkBranch;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffNip() {
		return staffNip;
	}
	public void setStaffNip(String staffNip) {
		this.staffNip = staffNip;
	}
	public String getStaffBranch() {
		return staffBranch;
	}
	public void setStaffBranch(String staffBranch) {
		this.staffBranch = staffBranch;
	}
	public String getStaffAccountNum() {
		return staffAccountNum;
	}
	public void setStaffAccountNum(String staffAccountNum) {
		this.staffAccountNum = staffAccountNum;
	}
	public ArrayList<HashMap<String, String>> getListApplication() {
		return listApplication;
	}
	public void setListApplication(ArrayList<HashMap<String, String>> listApplication) {
		this.listApplication = listApplication;
	}
	
}
