package form;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerForm extends BaseForm{
	private String custId;
	private String custName;
	private String custFullName;
	private String custIdType;
	private String custIdNumber;
	private String custIdExp;
	private String custGender;
	private String custPlaceOfBirth;
	private String custCityOfBirth;
	private String custProvinceOfBirth;
	private String custDateOfBirth;
	private String custMaritalStatus;
	private String custNumOfDependents;
	private String custLvlOfEducation;
	private String custMotherMaiden;
	private String searchCustName;
	private String searchCustFullName;
	private String searchCustIdNumber;
	private String searchCustDateOfBirth;
	
	private String custSpouseName;
	private String custSpouseId;
	private String custSpouseDob;
	private String custSpousePA;
	
	private String mailingAddress;
	private String custAddress1;
	private String custAddress2;
	private String custAddress3;
	private String custAddress4;
	private String custAdrsCity;
	private String custAdrsProvince;
	private String custAdrsZipCode;
	private String custAdrsKelurahan;
	private String custAdrsKecamatan;
	private String custAdrsHomePhone;
	private String custAdrsMobilePhone;
	private String custAdrsEmail;
	private String custAdrsResStatus;
	private String custAdrsLos;
	
	private String refNo;
	private String refferalBranch;
	private String dateReceived;
	private String facility;
	private String applicationPurpose;
	private String businessSource;
	private String mediaChannel;
	private String feeBranch;
	private String provisionBranch;
	private String kckkBranch;
	private String staffName;
	private String staffNip;
	private String staffBranch;
	private String staffAccountNum;
	private String status;
	private String holdBy;
	
	private ArrayList<HashMap<String, String>> listCustomer;
	private ArrayList<HashMap<String, String>> listSpouse;
	private ArrayList<HashMap<String, String>> listCustomerAddress;
	
	public String getCustSpouseId() {
		return custSpouseId;
	}
	public void setCustSpouseId(String custSpouseId) {
		this.custSpouseId = custSpouseId;
	}
	public String getCustSpouseDob() {
		return custSpouseDob;
	}
	public void setCustSpouseDob(String custSpouseDob) {
		this.custSpouseDob = custSpouseDob;
	}
	public String getCustSpousePA() {
		return custSpousePA;
	}
	public void setCustSpousePA(String custSpousePA) {
		this.custSpousePA = custSpousePA;
	}
	public String getMailingAddress() {
		return mailingAddress;
	}
	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}
	public String getCustAddress1() {
		return custAddress1;
	}
	public void setCustAddress1(String custAddress1) {
		this.custAddress1 = custAddress1;
	}
	public String getCustAddress2() {
		return custAddress2;
	}
	public void setCustAddress2(String custAddress2) {
		this.custAddress2 = custAddress2;
	}
	public String getCustAddress3() {
		return custAddress3;
	}
	public void setCustAddress3(String custAddress3) {
		this.custAddress3 = custAddress3;
	}
	public String getCustAddress4() {
		return custAddress4;
	}
	public String getCustAdrsResStatus() {
		return custAdrsResStatus;
	}
	public void setCustAdrsResStatus(String custAdrsResStatus) {
		this.custAdrsResStatus = custAdrsResStatus;
	}
	public void setCustAddress4(String custAddress4) {
		this.custAddress4 = custAddress4;
	}
	public String getCustAdrsCity() {
		return custAdrsCity;
	}
	public void setCustAdrsCity(String custAdrsCity) {
		this.custAdrsCity = custAdrsCity;
	}
	public String getCustAdrsProvince() {
		return custAdrsProvince;
	}
	public void setCustAdrsProvince(String custAdrsProvince) {
		this.custAdrsProvince = custAdrsProvince;
	}
	public String getCustAdrsZipCode() {
		return custAdrsZipCode;
	}
	public void setCustAdrsZipCode(String custAdrsZipCode) {
		this.custAdrsZipCode = custAdrsZipCode;
	}
	public String getCustAdrsKelurahan() {
		return custAdrsKelurahan;
	}
	public void setCustAdrsKelurahan(String custAdrsKelurahan) {
		this.custAdrsKelurahan = custAdrsKelurahan;
	}
	public String getCustAdrsHomePhone() {
		return custAdrsHomePhone;
	}
	public void setCustAdrsHomePhone(String custAdrsHomePhone) {
		this.custAdrsHomePhone = custAdrsHomePhone;
	}
	public String getCustAdrsEmail() {
		return custAdrsEmail;
	}
	public void setCustAdrsEmail(String custAdrsEmail) {
		this.custAdrsEmail = custAdrsEmail;
	}
	public String getCustAdrsLos() {
		return custAdrsLos;
	}
	public void setCustAdrsLos(String custAdrsLos) {
		this.custAdrsLos = custAdrsLos;
	}
	public ArrayList<HashMap<String, String>> getListSpouse() {
		return listSpouse;
	}
	public void setListSpouse(ArrayList<HashMap<String, String>> listSpouse) {
		this.listSpouse = listSpouse;
	}
	public ArrayList<HashMap<String, String>> getListCustomerAddress() {
		return listCustomerAddress;
	}
	public void setListCustomerAddress(ArrayList<HashMap<String, String>> listCustomerAddress) {
		this.listCustomerAddress = listCustomerAddress;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustFullName() {
		return custFullName;
	}
	public void setCustFullName(String custFullName) {
		this.custFullName = custFullName;
	}
	public String getCustIdType() {
		return custIdType;
	}
	public void setCustIdType(String custIdType) {
		this.custIdType = custIdType;
	}
	public String getCustIdNumber() {
		return custIdNumber;
	}
	public void setCustIdNumber(String custIdNumber) {
		this.custIdNumber = custIdNumber;
	}
	public String getCustIdExp() {
		return custIdExp;
	}
	public void setCustIdExp(String custIdExp) {
		this.custIdExp = custIdExp;
	}
	public String getCustGender() {
		return custGender;
	}
	public void setCustGender(String custGender) {
		this.custGender = custGender;
	}
	public String getCustPlaceOfBirth() {
		return custPlaceOfBirth;
	}
	public void setCustPlaceOfBirth(String custPlaceOfBirth) {
		this.custPlaceOfBirth = custPlaceOfBirth;
	}
	public String getCustCityOfBirth() {
		return custCityOfBirth;
	}
	public String getCustAdrsKecamatan() {
		return custAdrsKecamatan;
	}
	public void setCustAdrsKecamatan(String custAdrsKecamatan) {
		this.custAdrsKecamatan = custAdrsKecamatan;
	}
	public String getCustAdrsMobilePhone() {
		return custAdrsMobilePhone;
	}
	public void setCustAdrsMobilePhone(String custAdrsMobilePhone) {
		this.custAdrsMobilePhone = custAdrsMobilePhone;
	}
	public void setCustCityOfBirth(String custCityOfBirth) {
		this.custCityOfBirth = custCityOfBirth;
	}
	public String getCustProvinceOfBirth() {
		return custProvinceOfBirth;
	}
	public void setCustProvinceOfBirth(String custProvinceOfBirth) {
		this.custProvinceOfBirth = custProvinceOfBirth;
	}
	public String getCustDateOfBirth() {
		return custDateOfBirth;
	}
	public void setCustDateOfBirth(String custDateOfBirth) {
		this.custDateOfBirth = custDateOfBirth;
	}
	public String getCustMaritalStatus() {
		return custMaritalStatus;
	}
	public void setCustMaritalStatus(String custMaritalStatus) {
		this.custMaritalStatus = custMaritalStatus;
	}
	public String getCustNumOfDependents() {	
		return custNumOfDependents;
	}
	public void setCustNumOfDependents(String custNumOfDependents) {
		this.custNumOfDependents = custNumOfDependents;
	}
	public String getCustLvlOfEducation() {
		return custLvlOfEducation;
	}
	public void setCustLvlOfEducation(String custLvlOfEducation) {
		this.custLvlOfEducation = custLvlOfEducation;
	}
	public String getCustMotherMaiden() {
		return custMotherMaiden;
	}
	public void setCustMotherMaiden(String custMotherMaiden) {
		this.custMotherMaiden = custMotherMaiden;
	}
	public ArrayList<HashMap<String, String>> getListCustomer() {
		return listCustomer;
	}
	public void setListCustomer(ArrayList<HashMap<String, String>> listCustomer) {
		this.listCustomer = listCustomer;
	}
	public String getSearchCustName() {
		return searchCustName;
	}
	public void setSearchCustName(String searchCustName) {
		this.searchCustName = searchCustName;
	}
	public String getSearchCustFullName() {
		return searchCustFullName;
	}
	public void setSearchCustFullName(String searchCustFullName) {
		this.searchCustFullName = searchCustFullName;
	}
	public String getSearchCustIdNumber() {
		return searchCustIdNumber;
	}
	public void setSearchCustIdNumber(String searchCustIdNumber) {
		this.searchCustIdNumber = searchCustIdNumber;
	}
	public String getSearchCustDateOfBirth() {
		return searchCustDateOfBirth;
	}
	public void setSearchCustDateOfBirth(String searchCustDateOfBirth) {
		this.searchCustDateOfBirth = searchCustDateOfBirth;
	}
	public String getCustSpouseName() {
		return custSpouseName;
	}
	public void setCustSpouseName(String custSpouseName) {
		this.custSpouseName = custSpouseName;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getRefferalBranch() {
		return refferalBranch;
	}
	public void setRefferalBranch(String refferalBranch) {
		this.refferalBranch = refferalBranch;
	}
	public String getDateReceived() {
		return dateReceived;
	}
	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getApplicationPurpose() {
		return applicationPurpose;
	}
	public void setApplicationPurpose(String applicationPurpose) {
		this.applicationPurpose = applicationPurpose;
	}
	public String getBusinessSource() {
		return businessSource;
	}
	public void setBusinessSource(String businessSource) {
		this.businessSource = businessSource;
	}
	public String getMediaChannel() {
		return mediaChannel;
	}
	public void setMediaChannel(String mediaChannel) {
		this.mediaChannel = mediaChannel;
	}
	public String getFeeBranch() {
		return feeBranch;
	}
	public void setFeeBranch(String feeBranch) {
		this.feeBranch = feeBranch;
	}
	public String getProvisionBranch() {
		return provisionBranch;
	}
	public void setProvisionBranch(String provisionBranch) {
		this.provisionBranch = provisionBranch;
	}
	public String getKckkBranch() {
		return kckkBranch;
	}
	public void setKckkBranch(String kckkBranch) {
		this.kckkBranch = kckkBranch;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffNip() {
		return staffNip;
	}
	public void setStaffNip(String staffNip) {
		this.staffNip = staffNip;
	}
	public String getStaffBranch() {
		return staffBranch;
	}
	public void setStaffBranch(String staffBranch) {
		this.staffBranch = staffBranch;
	}
	public String getStaffAccountNum() {
		return staffAccountNum;
	}
	public void setStaffAccountNum(String staffAccountNum) {
		this.staffAccountNum = staffAccountNum;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHoldBy() {
		return holdBy;
	}
	public void setHoldBy(String holdBy) {
		this.holdBy = holdBy;
	}
	
	
	
}
