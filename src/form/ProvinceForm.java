package form;

import java.util.ArrayList;
import java.util.HashMap;

public class ProvinceForm extends BaseForm {
	private String provinceId;
	private String provinceName;
	private String searchProvinceName;
	private String cityName;
	private String cityId;
	private String field;
	private ArrayList<HashMap<String, String>> listProvince;
	
	public String getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getSearchProvinceName() {
		return searchProvinceName;
	}
	public void setSearchProvinceName(String searchProvinceName) {
		this.searchProvinceName = searchProvinceName;
	}
	public ArrayList<HashMap<String, String>> getListProvince() {
		return listProvince;
	}
	public void setListProvince(ArrayList<HashMap<String, String>> listProvince) {
		this.listProvince = listProvince;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	
}
