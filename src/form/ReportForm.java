package form;

public class ReportForm extends BaseForm {
	private String month;
	private String year;
	private String radioLaporanApp;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getRadioLaporanApp() {
		return radioLaporanApp;
	}
	public void setRadioLaporanApp(String radioLaporanApp) {
		this.radioLaporanApp = radioLaporanApp;
	}
}
