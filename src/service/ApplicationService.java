package service;

import dao.ApplicationDao;
import form.ApplicationForm;

public class ApplicationService {
	public void insertApplication(ApplicationForm frm) {
		ApplicationDao dao = new ApplicationDao();
		try {
			dao.insertApplication(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getApplicationById(ApplicationForm frm) {
		ApplicationDao dao = new ApplicationDao();
		try {
			dao.getApplicationById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getListApplication(ApplicationForm frm) {
		ApplicationDao dao = new ApplicationDao();
		try {
			frm.setListApplication(dao.getListAllApplication());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getSearchApplication(ApplicationForm frm) {
		ApplicationDao dao = new ApplicationDao();
		try {
			frm.setListApplication(dao.getSearchApplication(frm));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getNumberReff(ApplicationForm frm) {
		ApplicationDao dao = new ApplicationDao();
		Integer num = 0;
		String temp = null;
		try {
			num = dao.getRowNumApplication();
			String date = frm.getDateReceived();
			String year = date.substring(date.length()-2);
			String leadingZero = String.format("%05d", num);
			temp = "0960/"+"500/"+leadingZero+"/"+year;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}
	public String getNumberReff2(ApplicationForm frm) {
		ApplicationDao dao = new ApplicationDao();
		String temp = null;
		try {
			Integer num = Integer.parseInt(dao.getRowNumApplication2());
			String date = frm.getDateReceived();
			String year = date.substring(date.length()-2);
			String leadingZero = String.format("%05d", num);
			temp = "0960/"+"500/"+leadingZero+"/"+year;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}
}
