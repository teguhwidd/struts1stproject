package service;

import dao.CityDao;
import form.CityForm;

public class CityService {
	public void insertCity(CityForm newCity ) {
		CityDao dao = new CityDao();
		try {
			dao.insertCity(newCity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateCity(CityForm frm ) {
		CityDao dao = new CityDao();
		try {
			dao.updateCity(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void deleteCity(CityForm frm ) {
		CityDao dao = new CityDao();
		try {
			dao.deleteCity(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getCityById(CityForm frm) {
		CityDao dao = new CityDao();
		try {
			dao.getCityById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getListCity(CityForm frm) {
		CityDao dao = new CityDao();
		try {
			frm.setListCity(dao.getListAllCity());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getSearchCity(CityForm frm) {
		CityDao dao = new CityDao();
		try {
			frm.setListCity(dao.getSearchCity(frm));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
