package service;

import dao.ApplicationDao;
import dao.CustomerDao;
import dao.ProvinceDao;
import form.CustomerForm;

public class CustomerService {
	public void insertCustomer(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			dao.insertCustomer(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void insertCustomerSpouse(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			dao.insertCustomerSpouse(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void insertCustomerAdrs(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			dao.insertCustomerAddress(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getCustId(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		Integer temp = 0;
		try {
			temp = dao.getRowNumCustomer();
			frm.setCustId(Integer.toString(temp));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getCustomerById(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			dao.getCustomerById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getCustomerSpouseById(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			dao.getCustomerSpouseById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getCustomerAdrsById(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			dao.getCustomerAdrsById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getListCustomer(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			frm.setListCustomer(dao.getListAllCustumer());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getListProvince(CustomerForm frm) {
		ProvinceDao dao = new ProvinceDao();
		try {
			frm.setListCustomer(dao.getListAllProvince());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getListApplication(CustomerForm frm) {
		ApplicationDao dao = new ApplicationDao();
		try {
			frm.setListCustomer(dao.getListAllApplication());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getSearchCustomer(CustomerForm frm) {
		CustomerDao dao = new CustomerDao();
		try {
			frm.setListCustomer(dao.getSearchCustomer(frm));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
