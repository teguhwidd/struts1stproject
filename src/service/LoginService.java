package service;

import dao.LoginDao;
import form.LoginForm;

public class LoginService {
	public boolean dologin(LoginForm frm) {
		boolean retval = false;
		
		LoginDao dao = new LoginDao();
		try {
			retval = dao.isUserExist(frm.getUserId(), frm.getUserPassword());
			//session.setAttribute("userName", frm.getUser_id());
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("========RETEVAL "+retval);
		return retval;
	}
	public void getUserById(LoginForm frm) {
		LoginDao dao = new LoginDao();
		try {
			dao.getUserById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
