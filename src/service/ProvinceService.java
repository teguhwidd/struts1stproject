package service;

import dao.ProvinceDao;
import form.ProvinceForm;

public class ProvinceService {
	public void insertProvince(ProvinceForm frm ) {
		ProvinceDao dao = new ProvinceDao();
		try {
			dao.insertPrvince(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateProvince(ProvinceForm frm ) {
		ProvinceDao dao = new ProvinceDao();
		try {
			dao.updatePrvince(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void deleteProvince(ProvinceForm frm ) {
		ProvinceDao dao = new ProvinceDao();
		try {
			dao.deletePrvince(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getProvinceById(ProvinceForm frm) {
		ProvinceDao dao = new ProvinceDao();
		try {
			dao.getProvinceById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getListProvince(ProvinceForm frm) {
		ProvinceDao dao = new ProvinceDao();
		try {
			frm.setListProvince(dao.getListAllProvince());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getSearchProvince(ProvinceForm frm) {
		ProvinceDao dao = new ProvinceDao();
		try {
			frm.setListProvince(dao.getSearchProvince(frm));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
