package service;

import java.sql.Connection;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.ReportEngine;

import setting.DBConnection;

public class ReportService {
	
	public static String rptDesign = "F:/teguh/Java/eclipse-workspace/Struts1stProject/WebContent/report/reportAplikasi.rptdesign";
	public static String outputFile = "F:/teguh/Java/eclipse-workspace/Struts1stProject/WebContent/report/reportAplikasi.pdf";
	
	@SuppressWarnings("unchecked")
	public static void getReport(String montYear) {
		Connection conn = null;
		ReportEngine engine = null;
		EngineConfig config = new EngineConfig();
		try {
			Platform.startup(config);
			engine = new ReportEngine(config);
			
			IReportRunnable design = null;
			design = engine.openReportDesign(rptDesign);
			
			IRunAndRenderTask task = engine.createRunAndRenderTask(design);
			conn = DBConnection.getConReport();
			System.out.println("=================="+conn);
			task.getAppContext().put("OdaJDNCDriverPassInconnection", conn);
			
			task.setParameterValue("bulan_tahun", montYear);
			task.validateParameters();
			
			final HTMLRenderOption HTML_OPTIONS = new HTMLRenderOption();
			HTML_OPTIONS.setOutputFileName(outputFile);
			HTML_OPTIONS.setOutputFormat("pdf");
			task.setRenderOption(HTML_OPTIONS);
			task.run();
			task.close();
			engine.destroy();
			
			System.out.println("Report is generated successfully");			
		}
		catch(final Exception ex) {
			ex.printStackTrace();
		}
		finally {
			Platform.shutdown();
		}
	}

}
