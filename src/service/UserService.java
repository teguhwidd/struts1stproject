package service;

import dao.UserDao;
import form.UserForm;

public class UserService {
	
	public void insertUser(UserForm newUser ) {
		UserDao dao = new UserDao();
		try {
			dao.insertUser(newUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getUserById(UserForm frm) {
		UserDao dao = new UserDao();
		try {
			dao.getUserById(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateUser(UserForm frm ) {
		UserDao dao = new UserDao();
		try {
			dao.updateUser(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void deleteUser(UserForm frm ) {
		UserDao dao = new UserDao();
		try {
			dao.deleteUser(frm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getListUser(UserForm frm) {
		UserDao dao = new UserDao();
		try {
			frm.setListUser(dao.getListAllUser());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void getSearchUser(UserForm frm) {
		UserDao dao = new UserDao();
		try {
			frm.setListUser(dao.getSearchUser(frm));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
