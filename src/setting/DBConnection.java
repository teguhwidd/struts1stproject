package setting;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBConnection {
//	public static String DS_NAME = "java:jboss/datasources/ds_oracle_hr"; //for jboss
	public static String DS_NAME = "ds_oracle_hr"; //for WebLogic
	
	public static Connection getConnection() throws Exception{
		Properties prop = new Properties();
		Connection conn = null;
		InitialContext ctx = new InitialContext();
		try {
			prop.load(new FileInputStream("C:\\properties\\con_Struts1st_Project.properties"));
			
		} catch (IOException ex) {
			System.out.println("Gagal load file properties");
			ex.printStackTrace();
		}
		try {
			DataSource ds = (DataSource)ctx.lookup(prop.getProperty("dsName"));
			System.out.println("DS_NAME ====="+prop.getProperty("dsName"));
			conn = ds.getConnection();
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			ctx.close();
		}
		return conn;
	}
	
	public static Connection getConnection1() throws Exception{
		Connection conn = null;
		InitialContext ctx = new InitialContext();
		try {
			DataSource ds = (DataSource)ctx.lookup(DS_NAME);
			conn = ds.getConnection();
		}catch(SQLException ex) {
			ex.printStackTrace();
		}finally {
			ctx.close();
		}
		return conn;
	}
	
	public static Connection getConnection2() throws Exception{
		Connection conn = null;
		
		String hostname = "localhost";
		String port = "1521";
		String sid = "xe";
		String username = "hr";
		String password = "12345";
		
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			conn = DriverManager.getConnection("jdbc:oracle:thin:@" + hostname + ":" + port + ":" + sid, username, password);
			
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
		return conn;
	}
	
	public static Connection getConnection3(String dsName) throws Exception{
		Connection conn = null;
		InitialContext ctx = new InitialContext();
		try {
			DataSource ds = (DataSource)ctx.lookup(dsName);
			conn = ds.getConnection();
		}catch(SQLException ex) {
			ex.printStackTrace();
		}finally {
			ctx.close();
		}
		return conn;
	}
	
	public static Connection getConReport() throws Exception{
		Connection conn = null;
		
		String hostname = "localhost";
		String port = "1521";
		String sid = "xe";
		String username = "hr";
		String password = "12345";
		
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			conn = DriverManager.getConnection("jdbc:oracle:thin:@" + hostname + ":" + port + ":" + sid, username, password);
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
		return conn;
	}
	
}
