package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class CreateFileProperties {

	public static void main(String[] args) {
		Properties properties = new Properties();
		 
        //Misalnya kita mau buat file konfigurasi untuk DBMS MySQL
        properties.setProperty("dsName", "ds_oracle_hr");
        properties.setProperty("forWebLogic", "ds_oracle_hr");
        properties.setProperty("forJBoss", "java:jboss/datasources/ds_oracle_hr");
        try {
            //Memyimpan file konfigurasi dengan nama file.properties
            properties.store(new FileOutputStream("C:\\properties\\con_Struts1st_Project.properties"), "Database Konfigurasi JAVA WebLogic & JBoss");
            System.out.println("Create File Properties Success !");
            try {
            	readFileProperties();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        } catch (IOException ex) {
            System.out.println("Gagal membuat file database.properties");
            ex.printStackTrace();
        }
	}
	
	public static void readFileProperties() throws Exception{
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("C:\\properties\\con_Struts1st_Project.properties"));
			System.out.println("DS_NAME ====="+prop.getProperty("dsName"));
			
		} catch (IOException ex) {
			System.out.println("Gagal load file properties");
			ex.printStackTrace();
		}
	}
	

}
