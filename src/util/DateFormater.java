package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFormater {
	public static String getCurDate(String formatDate) {
		DateFormat dateFormat = new SimpleDateFormat(formatDate);//"yyyy/MM/dd HH:mm:ss"
		Calendar cal = Calendar.getInstance();
		System.out.println(dateFormat.format(cal.getTime()));
		return dateFormat.format(cal.getTime());
	}
	public static java.sql.Date getCurSqlDate() {
	    Date today = new Date();
	    return new java.sql.Date(today.getTime());
	}
	public static String dateToStr(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);//("yyyy-MM-dd HH:mm:ssZ");
        String finalFormat = dateFormat.format(date);
		return finalFormat;
	}
	public static java.sql.Date strToSqlDate(String date){
//		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date temp = new Date();
		try {
			temp = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new java.sql.Date(temp.getTime());
	}
	public static String getYear(String date) {
		String temp = date.substring(date.length()-2);
		return temp;
	}
}
